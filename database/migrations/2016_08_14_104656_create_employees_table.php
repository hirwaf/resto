<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->enum('gender', ['female', 'male']);
            $table->string('nation_id',20)->unique();
            $table->string('telephone',15)->unique();
            $table->string('email')->unique();
            $table->string('post')->nullable();
            $table->boolean('chef')->default(false);
            $table->text('note')->nullable();
            $table->text('social_networks')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}

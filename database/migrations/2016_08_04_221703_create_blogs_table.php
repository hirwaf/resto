<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('excerpt',255);
            $table->string('slug')->unique();
            $table->text('content');
            $table->string('image');
            $table->string('tags')->nullable();
            $table->boolean('from_net')->default(false);
            $table->string('source')->nullable();
            $table->string('author')->nullable();
            $table->boolean('published');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');
    }
}

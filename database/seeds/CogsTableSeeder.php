<?php

use Illuminate\Database\Seeder;
use App\Cog;

class CogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cog::create([
        	'name'	=>	'name',
        	'value'	=>	'toundra coffee cup',
        	'valid'	=>	true,
        ]);

        Cog::create([
        	'name'	=>	'icon',
        	'value'	=>	'favicon.jpg',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'email',
        	'value'	=>	'info@toundra.com',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'booking',
        	'value'	=>	'250788888888',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'care',
        	'value'	=>	'250788888888',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'facebook',
        	'value'	=>	'',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'pinterest',
        	'value'	=>	'',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'instagram',
        	'value'	=>	'',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'days',
        	'value'	=>	'07 - 22',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'suturday',
        	'value'	=>	'07 - 00',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'sunday',
        	'value'	=>	'08 - 21',
        	'valid'	=>	true
        ]);

        Cog::create([
        	'name'	=>	'description',
        	'value'	=>	'',
        	'valid'	=>	true
        ]);
    }
}

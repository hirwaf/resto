<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
        	'name'		=>	'Home',
        	'slug'		=>	'welcome',
        	'position'	=>	1,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
        Page::create([
        	'name'		=>	'Menu',
        	'slug'		=>	'menu',
        	'position'	=>	2,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
        Page::create([
        	'name'		=>	'Events',
        	'slug'		=>	'event',
        	'position'	=>	3,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
        Page::create([
        	'name'		=>	'Gallery',
        	'slug'		=>	'gallery',
        	'position'	=>	4,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
        Page::create([
        	'name'		=>	'Blog',
        	'slug'		=>	'blog',
        	'position'	=>	5,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
        Page::create([
        	'name'		=>	'About Us',
        	'slug'		=>	'about-us',
        	'position'	=>	6,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
        Page::create([
        	'name'		=>	'Contact Us',
        	'slug'		=>	'contact-us',
        	'position'	=>	7,
        	'subpage'	=>	json_encode(array()),
        	'enabled'	=>	true
        ]);
    }
}

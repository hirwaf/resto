<?php

use Illuminate\Database\Seeder;
use App\Page;
use App\Cog;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->pages();
        $this->cog();
    }


    protected function pages(){
        Page::create([
            'name'      =>  'Home',
            'slug'      =>  'welcome',
            'position'  =>  1,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
        Page::create([
            'name'      =>  'Menu',
            'slug'      =>  'menu',
            'position'  =>  2,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
        Page::create([
            'name'      =>  'Events',
            'slug'      =>  'event',
            'position'  =>  3,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
        Page::create([
            'name'      =>  'Gallery',
            'slug'      =>  'gallery',
            'position'  =>  4,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
        Page::create([
            'name'      =>  'Blog',
            'slug'      =>  'blog',
            'position'  =>  5,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
        Page::create([
            'name'      =>  'About Us',
            'slug'      =>  'about-us',
            'position'  =>  6,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
        Page::create([
            'name'      =>  'Contact Us',
            'slug'      =>  'contact-us',
            'position'  =>  7,
            'subpage'   =>  json_encode(array()),
            'enabled'   =>  true
        ]);
    } 

    protected function cog(){
        Cog::create([
            'name'  =>  'name',
            'value' =>  'toundra coffee cup',
            'valid' =>  true,
        ]);

        Cog::create([
            'name'  =>  'icon',
            'value' =>  'favicon.jpg',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'email',
            'value' =>  'info@toundra.com',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'booking',
            'value' =>  '250788888888',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'care',
            'value' =>  '250788888888',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'facebook',
            'value' =>  '',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'pinterest',
            'value' =>  '',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'instagram',
            'value' =>  '',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'days',
            'value' =>  '07 - 22',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'suturday',
            'value' =>  '07 - 00',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'sunday',
            'value' =>  '08 - 21',
            'valid' =>  true
        ]);

        Cog::create([
            'name'  =>  'description',
            'value' =>  '',
            'valid' =>  true
        ]);

        $styles = [
            'top'           =>  "#210C09",
            'nav'           =>  '#210C09',
            'menu'          =>  'menu.bg.jpg',
            'event'         =>  'event-bg.jpg',
            'gallery'       =>  'gallery-bg.jpg',
            'blog'          =>  'blog-bg.jpg',
            'about-us'      =>  '3_002.jpg',
            'contact-us'    =>  '3_002.jpg',
            'header-size'   =>  'small'
        ];

        Cog::create([
            'name'  =>  'styles',
            'value' =>  json_encode($styles),
            'valid' =>  true
        ]);
    }

}

<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Admin::create([
			'name'		=>	'hirwa felix',
			'email'		=>	'hirwa.felix@gmail.com',
			'username'	=>	'hirwaf',
			'telephone'	=>	'250789812404',
			'password'	=>	bcrypt('D@1'),
			'super'		=>	true
		]);

		Admin::create([
			'name'		=>	'mugemana pacific',
			'email'		=>	'ligana24@gmail.com',
			'username'	=>	'ligana',
			'telephone'	=>	'250700000000',
			'password'	=>	bcrypt('ligana'),
			'super'		=>	false
		]);

    }
}

<?php
$title = "Toundra Coffee Cup";
return 	array(

	'meta' => [
	
		'default_title' => $title,
		'concat_default_title' => false,
		'concat_with' => ' : '

	],

	'social' => [

		'og' => [
			'render' => true,
			'additional_og_tags' => []
		],
		
		'twitter' => [
			'render' => true
		]

	]
	
);
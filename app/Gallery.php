<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'category_id', 'url', 'published' 
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}

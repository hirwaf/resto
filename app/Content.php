<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //
    protected $fillable = [
        'page_id', 'page_meta', 'content'
    ];

    public function page()
    {
    	return $this->belongsTo('App\Page');
    }
    
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('welcome');
});

Route::get('/admin', function(){
    return redirect('admin/login');
});

Route::get('/blog/{slug}', [
    'as'    =>  'view.blog.web',
    'uses'  =>  'WebController@viewBlog'
]);
Route::get('/video/{id}', [
    'as'    =>  'view.video.web',
    'uses'  =>  'WebController@viewVideo'
]);
Route::post('/message/send', function(){
    return redirect('contact-us');
});
Route::post('/message/send','MessageController@send');

Route::get('/{slug}','WebController@getContent');


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
| Here is Admin Routes 
*/
Route::group(['prefix' => '/admin', 'namespace' => 'Admin' ], function(){
	
	Route::get('/',[
		'as'	=>	'login.admin',
		'uses'	=>	'AdminController@getLogin'
	]);
	Route::get('/login','AdminController@getLogin');

	Route::post('/login',[
		'as'	=>	'post.login.admin',
		'uses'	=>	'AdminController@login'
	]);

	Route::group(['middleware' => 'admin'], function(){
		Route::get('/dashboard',[
			'as'	=>	'dashboard.admin',
			'uses'	=>	'AdminController@dashboard'
		]);
		Route::get('/profile',[
			'as'	=>	'profile.admin',
			'uses'	=>	'AdminController@getProfile'
		]);
		Route::post('/profile',[
			'as'	=>	'post.profile.admin',
			'uses'	=>	'AdminController@postProfile'
		]);
		Route::get('/logout',[
			'as'	=>	'logout.admin',
			'uses'	=>	'AdminController@logout'
		]);

		Route::resource('pages', 'PagesController', [
            'names' =>  [
                'index'     =>  'pages.admin',
                'create'	=>	'create.page.admin',
				'store'		=>	'store.page.admin',
				'edit'		=>	'edit.page.admin',
				'show'		=>	'show.page.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);

		Route::resource('events', 'EventsController', [
            'names' =>  [
                'index'     =>  'events.admin',
                'create'	=>	'create.event.admin',
				'store'		=>	'store.event.admin',
				'edit'		=>	'edit.event.admin',
				'show'		=>	'show.event.admin',
                'destroy'   =>  'destroy.event.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);
        Route::post('events/destroy',[
            'as'    =>  'destroy.event.admin',
            'uses'  =>  'EventsController@destroy'
        ]);
        Route::post('events/update',[
            'as'    =>  'update.event.admin',
            'uses'  =>  'EventsController@update'
        ]);

        Route::resource('videos', 'VideoController', [
            'names' =>  [
                'index'     =>  'videos.admin',
                'create'	=>	'create.video.admin',
				'store'		=>	'store.video.admin',
				'edit'		=>	'edit.video.admin',
				'show'		=>	'show.video.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);

        Route::post('videos/update', [
            'as'    =>  'update.video.admin',
            'uses'  =>  'VideoController@update'
        ]);
        Route::post('videos/destroy', [
            'as'    =>  'destroy.video.admin',
            'uses'  =>  'VideoController@destroy'
        ]);

        Route::post('/pages', [
        	'as'	=>	'page.position.admin',
        	'uses'	=>	'PagesController@update'
        ]);

        Route::resource('menus', 'MenusController', [
            'names' =>  [
                'index'     =>  'menus.admin',
                'create'	=>	'create.menu.admin',
				'store'		=>	'store.menu.admin',
				'edit'		=>	'edit.menu.admin',
				'show'		=>	'show.menu.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);

        Route::resource('employees', 'EmployeesController', [
            'names' =>  [
                'index'     =>  'employees.admin',
                'create'	=>	'create.employee.admin',
				'store'		=>	'store.employee.admin',
				'edit'		=>	'edit.employee.admin',
				'show'		=>	'show.employee.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);

        Route::post('employees/update', [
            'as'    =>  'update.employee.admin',
            'uses'  =>  'EmployeesController@update'
        ]);

        Route::post('employees/destroy', [
            'as'    =>  'destroy.employee.admin',
            'uses'  =>  'EmployeesController@destroy'
        ]);

        Route::post('employee/restore', [
            'as'    =>  'restore.employee.admin',
            'uses'  =>  'EmployeesController@restore'
        ]);

        Route::resource('blogs', 'BlogsController', [
            'names' =>  [
                'index'     =>  'blogs.admin',
                'create'	=>	'create.blog.admin',
				'store'		=>	'store.blog.admin',
				'edit'		=>	'edit.blog.admin',
				'show'		=>	'show.blog.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);

        Route::post('blogs/update', [
            'as'    =>  'update.blog.admin',
            'uses'  =>  'BlogsController@update'
        ]);

        Route::post('blogs/destroy', [
            'as'    =>  'destroy.blog.admin',
            'uses'  =>  'BlogsController@destroy'
        ]);

        Route::resource('setting', 'CogsController', [
            'names' =>  [
                'index'     =>  'cogs.admin',
                'create'	=>	'create.cog.admin',
				'store'		=>	'store.cog.admin',
				'edit'		=>	'edit.cog.admin',
				'show'		=>	'show.cog.admin'
            ],
            'only'  =>  [
                'index','show','create','edit','store'
            ]
        ]);

        Route::get('/styles', [
            'as'    =>  'style.cogs.admin',
            'uses'  =>  'CogsController@style'
        ]);

        Route::post('/styles', [
            'as'    =>  'store.style.cogs.admin',
            'uses'  =>  'CogsController@storeStyles'
        ]);

        Route::get('/meuns/categories', [
        	'as'	=>	'cats.admin',
        	'uses'	=>	'MenusController@getCategories'
        ]);

        Route::post('/meuns/categories', [
        	'as'	=>	'post.cat.admin',
        	'uses'	=>	'MenusController@postCategory'
        ]);

        Route::post('/menus/update', [
        	'as'	=>	'update.menu.admin',
        	'uses'	=>	'MenusController@update'
        ]);

        Route::get("message/send/{type}/{id}/{token}", [
            'as'    =>  'create.massege.admin',
            'uses'  =>  'MessageController@create'
        ]);

        Route::get('messages', [
            'as'    =>  'toundra.inbox.admin',
            'uses'  =>  'MessageController@index'
        ]);

        Route::post("message/sendsms", [
            'as'    =>  'send.sms.message.admin',
            'uses'  =>  'MessageController@sendSms'
        ]);

        Route::post("message/sendemail", [
            'as'    =>  'send.email.message.admin',
            'uses'  =>  'MessageController@sendEmail'
        ]);

        Route::post("messages/read",[
            'as'    =>  'message.read.admin',
            'uses'  =>  'MessageController@readMessage'
        ]);



	});	

});

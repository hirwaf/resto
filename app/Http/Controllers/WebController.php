<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\PageRepository;
use App\Repositories\CogRepository;

class WebController extends Controller
{
    public 	$name,
    		$icon,
    		$email,
    		$booking,
    		$care,
    		$facebook,
    		$pinterest,
    		$instagram,
    		$day,
    		$suturday,
    		$sunday,
    		$description,
    		$styles;

    private $page,
    		$cog;
	function __construct(){
		$this->middleware('web');
		$this->metaData = new \Astroanu\SEOGen\Data();
		$this->cog = new CogRepository;
		$this->page = new PageRepository;
		// Get Site Informations
		$this->name = $this->cog->siteinfo('name');
		$this->icon = $this->cog->siteinfo('icon');
		$this->email = $this->cog->siteinfo('email');
		$this->booking = $this->cog->siteinfo('booking');
		$this->care = $this->cog->siteinfo('care');
		$this->facebook = $this->cog->siteinfo('facebook');
		$this->pinterest = $this->cog->siteinfo('pinterest');
		$this->instagram = $this->cog->siteinfo('instagram');
		$this->day = $this->cog->siteinfo('day');
		$this->suturday = $this->cog->siteinfo('suturday'); 
		$this->sunday = $this->cog->siteinfo('sunday');
		$this->description = $this->cog->siteinfo('description');
		
		$this->styles = json_decode($this->cog->siteinfo('styles'),true);

		$this->metaData->description = $this->description;
		$this->metaData->robots = 'noindex,nofollow';
		$this->metaData->image = url('images/web/icons/'.$this->icon);



	}

	public function getContent($slug='')
	{
		$slug = $slug == "" ? 'welcome' : $slug;
		if( $this->page->getSiteMenu() != null )
		{
			foreach ($this->page->getSiteMenu() as $page) {
				if( $page->slug == $slug )
				{
					$this->metaData->title = strtoupper($page->slug). " : ". strtoupper($this->name) ;
					return view($slug,[
						'menus'	=>	$this->page->getSiteMenu(),
						'info'	=> 	$this,
						'page'	=>	$this->page,
						'bg'	=>	$page->slug != 'welcome'? $this->styles[$page->slug] : $this->styles['menu'],
						'metaData' => $this->metaData,
					]);
					break;
				}
			}
		}
	}
	
	public function viewBlog($slug)
	{
		$blog = \App\Blog::where('slug',$slug)
						 ->get()
						 ->first();
		if ($blog) {
			$this->metaData->title = strtoupper($blog->title);
			$this->metaData->description = $blog->excerpt;
			return view('blogview',[
				'menus'	=>	$this->page->getSiteMenu(),
				'info'	=> 	$this,
				'page'	=>	$this->page,
				'bg'	=>	$this->styles['blog'],
				'metaData' 	=> 	$this->metaData,
				'blog'	=>	$blog
			]);
		}
		else
			abort(404);
	}
	public function viewVideo($id)
	{
		$blog = \App\Video::findOrFail($id);
		
		$this->metaData->title = strtoupper($blog->title);
		return view('videoview',[
			'menus'	=>	$this->page->getSiteMenu(),
			'info'	=> 	$this,
			'page'	=>	$this->page,
			'bg'	=>	$this->styles['event'],
			'metaData' 	=> 	$this->metaData,
			'video'	=>	$blog
		]);
	}

}

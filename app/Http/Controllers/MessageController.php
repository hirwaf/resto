<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;

class MessageController extends Controller
{
    //

	public function send(Request $request)
	{
		Message::create([
			'name' => $request->input('contact-name'), 
			'email' => $request->input('contact-email'), 
			'message' => $request->input('contact-message'),
			'read'		=> 	false,
			'seen'		=>	false
		]);

		return redirect()->back();
	}

}

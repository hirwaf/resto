<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\CogRepository;
use App\Repositories\PageRepository;

class CogsController extends Controller
{
    protected $admin,
              $cog,
              $page;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->cog = new CogRepository;
        $this->page = new PageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.layouts.cogs',[
            'get'   =>  $this->admin,
            'cog'  =>  $this->cog
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $this->cog->store($request);

        if( $store !== true ){
            session(['errors' => $store]);
            return redirect(route('cogs.admin'));
        }else{
            return redirect()->route('cogs.admin');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function style()
    {
        return view('admin.layouts.styles',[
            'get'   =>  $this->admin,
            'cog'   =>  $this->cog,
            'pages' =>  $this->page->getSiteMenu()
        ]);
    }

    public function storeStyles(Request $request)
    {
        $store = $this->cog->storestyles($request);
        if($store === true)
        {
            session(['success' => 'Successfully Stored' ]);
            return redirect(route('style.cogs.admin'));
        }
        else
        {
            session(['errors' => $store ]);
            return redirect(route('style.cogs.admin'))->withInput();
        }
    }

}

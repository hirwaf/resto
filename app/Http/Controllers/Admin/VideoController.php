<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\VideoRepository;

class VideoController extends Controller
{
    protected $admin,
              $repo;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->repo = new VideoRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.layouts.pages.videos',[
            'get'       =>  $this->admin,
            'title'     =>  "Add New",
            'edit'      =>  false,
            'button'    =>  "Save",
            'route'     =>  "store.video.admin",
            'videos'    =>  $this->repo->getAllVideos()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $this->repo->store($request);
        if ( $store === true ) {
            session([ 'success' => 'Video Successfully Save ' ]);
            return redirect(route('videos.admin'));   
        }
        else
        {
            session([ 'errors' => $store ]);
            return redirect(route('videos.admin'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.layouts.pages.videos',[
            'get'       =>  $this->admin,
            'title'     =>  "Edit",
            'edit'      =>  true,
            'button'    =>  "Save changes",
            'route'     =>  "update.video.admin",
            'video'     =>  $this->repo->getVideoById($id),
            'videos'    =>  $this->repo->getAllVideos()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $update = $this->repo->update($request);
        if ( $update === true ) {
            session([ 'success' => 'Video Successfully Updated ' ]);
            return redirect(route('videos.admin'));   
        }
        else
        {
            session([ 'errors' => $update ]);
            return redirect(route('videos.admin'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $destroy = $this->repo->destroy($request->id);
        if ( $destroy === true ) 
            session([ 'success' => 'Video Successfully Updated ' ]);
        else
            session([ 'errors' => "Delete Fail\nTry again later !!" ]);

        echo url(route('videos.admin'));
    }
}

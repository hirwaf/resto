<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Repositories\AdminRepository;

use Validator;
use Auth;


class AdminController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    //

	protected $guard = 'admins';


	function __construct()
	{
		//$this->middleware('admin');
	}

	public function getLogin()
	{	
		if( Auth::guard('admins')->check() ){
			return redirect(route('dashboard.admin'));
		}
		return view('admin.layouts.login');
	}


	public function dashboard(AdminRepository $repo)
	{
		$this->middleware('admin');
		return view('admin.layouts.dashboard',[
			'get'	=>	$repo
		]);
	}

	public function getProfile(AdminRepository $repo)
	{
		$this->middleware('admin');
		return view('admin.layouts.profile',[
			'get'	=>	$repo
		]);	
	}

	public function postProfile(Request $request,AdminRepository $repo)
	{
		$this->middleware('admin');
		if ($repo->update($request) === true ) {
			return redirect(route('profile.admin'));
		}
		else{
			session(['errors' => $repo->update($request)]);
			return redirect(route('profile.admin'));
		}
	}

	public function login(Request $request)
	{
		$username = $request->credential;
		$password = $request->password;

		$credentials = [
			'email' 	=> 	[
				'email'		=>	$username,
				'password'	=>	$password
			],
			'username'	=>	[
				'username'	=>	$username,
				'password'	=>	$password
			],
			'phone'	=>	[
				'telephone'	=>	$username,
				'password'	=>	$password
			]
		];

		if ( Auth::guard('admins')->attempt( $credentials['email'] ) ) {
			return redirect()->intended(route('dashboard.admin'));
		}
		elseif ( Auth::guard('admins')->attempt( $credentials['username'] ) ) {
			return redirect()->intended(route('dashboard.admin'));
		}
		elseif ( Auth::guard('admins')->attempt( $credentials['phone'] ) ) {
			return redirect()->intended(route('dashboard.admin'));
		}
		else{
			return redirect(route('login.admin'))
					->with('msg','Incorrect credentials')
					->withInput($request->except('password'));
		}
	}

	public function logout(){
		Auth::guard('admins')->logout();
		return redirect(route('login.admin'));
	}



}
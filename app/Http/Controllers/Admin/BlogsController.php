<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\BlogRepository;


class BlogsController extends Controller
{
    protected $admin,
              $repo;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->repo = new BlogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.layouts.pages.blogs',[
            'get'       =>  $this->admin,
            'articles'  =>  $this->repo->getAllBlog(),
            'drafts'    =>  $this->repo->getDraftedBlog(),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.layouts.pages.addblog',[
            'get'   =>  $this->admin,
            'title' =>  "Add New",
            'edit'  =>  false,
            'button'=>  "Publish",
            'route' =>  "store.blog.admin"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $this->repo->store($request);
        if ( $store === true )
        {
            session([ 'success' => 'Article Successfully Saved' ]);
            return redirect(route('create.blog.admin'));
        }
        else
        {
            session([ 'errors' => $store ]);
            return redirect(route('create.blog.admin'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.layouts.pages.addblog',[
            'get'   =>  $this->admin,
            'title' =>  "Edit ",
            'edit'  =>  true,
            'button'=>  "Save changes",
            'blog'  =>  $this->repo->getBlogById($id),
            'route' =>  "update.blog.admin"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $update = $this->repo->update($request);
        if ( $update === true )
        {
            session([ 'success' => 'Article Successfully Upated' ]);
            return redirect(route('blogs.admin'));
        }
        else
        {
            session([ 'errors' => $update ]);
            return redirect(route('edit.blog.admin',$request->id))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $perm = $request->has('perm') ? false : true;
        $delete = $this->repo->destroy($request->id,$perm);
        
        echo url(route('blogs.admin'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\EmployeeRepository;

class MessageController extends Controller
{
    protected $admin,
              $repo;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->repo = new EmployeeRepository;
    }

    public function create($type,$id,$token)
    {
    	if( $token === session()->pull('token_msg') )
    	{
    		return view('admin.message',[
	            'get'       =>  $this->admin,
	            'type'		=>	$type,
	            'route'		=>	$type == 'sms' ? "send.sms.message.admin" : 'send.email.message.admin',
	            'employee'	=>	$this->repo->getEmployeeById($id)
	        ]);
    	}
    	else
    		return redirect(route('employees.admin'));
    }

    public function sendSms(Request $request)
    {

    }

    public function sendEmail(Request $request)
    {

    }

    public function readMessage(Request $request)
    {
        $this->admin->makeAsRead($request->id);
        echo true;
    }


    public function index()
    {
        return view('admin.messages',[
            'get'       =>  $this->admin
        ]);
    }

}

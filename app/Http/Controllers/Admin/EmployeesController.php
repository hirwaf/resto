<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\EmployeeRepository;

class EmployeesController extends Controller
{
    protected $admin,
              $repo;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->repo = new EmployeeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['token_msg' => str_random(40) ]);
        return view('admin.layouts.pages.employee',[
            'get'       =>  $this->admin,
            'employees' =>  $this->repo->getAllEmployees(),
            'history'   =>  $this->repo->getHiredEmployees(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.layouts.pages.addemployee',[
            'get'       =>  $this->admin,
            'route'     =>  'store.employee.admin',
            'title'     =>  'Add New',
            'button'    =>  'Add New',
            'edit'      =>  false
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $this->repo->store($request);
        if ( $store === true )
        {
            session([ 'success' => $request->fistname.' Successfully Added .' ]);
            return redirect(route('employees.admin'));
        }
        else{
            session([ 'errors' => $store ]);
            return redirect(route('create.employee.admin'))->withInput();  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.layouts.pages.addemployee',[
            'get'       =>  $this->admin,
            'route'     =>  'update.employee.admin',
            'title'     =>  'Edit',
            'button'    =>  'Save changes',
            'edit'      =>  true,
            'employee'  =>  $this->repo->getEmployeeById($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $update = $this->repo->update($request);
        if ( $update === true )
        {
            session([ 'success' => $request->fistname.' Successfully Updated .' ]);
            return redirect(route('employees.admin'));
        }
        else{
            session([ 'errors' => $update ]);
            return redirect(route('edit.employee.admin', $request->id))->withInput();   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $type = $request->type ? $request->type : 'n'; 
        $destroy = $this->repo->destroy($request->id, $type);

        if( $destroy === false )
            session( [ 'errors' => 'Fail Try Aigan Later !' ] );

        echo url(route('employees.admin'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        $restore = $this->repo->restore($request->id);

        if( $restore === false )
            session( [ 'errors' => 'Fail Try Aigan Later !' ] );

        echo url(route('employees.admin'));
    }
}

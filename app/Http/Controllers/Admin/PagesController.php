<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\PageRepository;

class PagesController extends Controller
{
    protected $admin,
              $pages;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->pages = new PageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['page_edit'=>str_random(40)]);
        return view('admin.layouts.pages.index',[
            'get'   =>  $this->admin,
            'not'   =>  ['blog','menu','contact-us','event'],
            'pages' =>  $this->pages->getPages()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $token = $request->get('_dt') == session()->pull('page_edit') ? true : false;
        $page = \App\Page::findOrFail($id);
        return view('admin.layouts.pages.edit',[
            'get'   =>  $this->admin,
            'token' =>  $token,
            'slug'  =>  $page->slug,
            'page'  =>  $page
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageRepository $page)
    {
        if( $request->has('where') ){
            if($request->where == 'p'){
                $page->changePosition($request);
            }
            elseif ( $request->where == 's' ){
                $page->changeStatus($request);
            }
            elseif ( $request->where == 'w' ){
                $page->changeSlides($request);
            }
            elseif ( $request->where == 'u' ) {
                $page->updateAboutUs($request);
            }
            elseif ( $request->where == 'g' ) {
                $page->storeGallery($request);
            }
            elseif ( $request->where == 'di' ) {
                $page->deleteGallery($request->id);
            }
            return redirect(route('pages.admin'));
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

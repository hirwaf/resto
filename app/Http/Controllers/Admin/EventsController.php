<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\EventRepository;

class EventsController extends Controller
{
    protected $admin,
              $repo;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
        $this->repo = new EventRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.layouts.pages.events',[
            'get'       =>  $this->admin,
            'route'     => 'store.event.admin',
            'title'     =>  'Add New',
            'button'    =>  'Add New',
            'edit'      =>  false,
            'events'    =>  $this->repo->getAllEvents(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $this->repo->store($request);

        if ( $store === true ){
            session([ 'success' => ucfirst($request->title)." Successfully Added ." ]);
            return redirect(route('events.admin'));
        }
        else
        {
            session([ 'errors' => $store ]);
            return redirect(route('events.admin'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.layouts.pages.events',[
            'get'       =>  $this->admin,
            'route'     => 'update.event.admin',
            'title'     =>  'Edit',
            'button'    =>  'Edit',
            'edit'      =>  true,
            'event'     =>  $this->repo->getEventById($id),
            'events'    =>  $this->repo->getAllEvents()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $update = $this->repo->update($request);

        if ( $update === true ){
            session([ 'success' => ucfirst($request->title)." Successfully Updated ." ]);
            return redirect(route('events.admin'));
        }
        else
        {
            session([ 'errors' => $update ]);
            return redirect(route('edit.event.admin',$request->id))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $destroy = $this->repo->destroy($request->id);
        return url(route('events.admin'));
    }
}

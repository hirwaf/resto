<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AdminRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\MenuRepository;

class MenusController extends Controller
{
    protected $admin,
              $pages;

    public function __construct()
    {
        $this->middleware('admin');
        $this->admin = new AdminRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( MenuRepository $menu )
    {
        return view('admin.layouts.pages.items', [
            'get'   =>  $this->admin,
            'items' =>  $menu->getItemsList(),
            'title'     =>  'Add New',
            'button'    =>  'Add New',
            'edit'      =>  false,
            'route'     =>  'store.menu.admin',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request, MenuRepository $menu )
    {
        $store = $menu->store( $request );
        if ( $store === true ) 
            return redirect(route('menus.admin'));
        else
        {   
            session(['errors' => $store ]);
            return redirect(route('menus.admin'))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id , MenuRepository $menu )
    {
        return view('admin.layouts.pages.items', [
            'get'       =>  $this->admin,
            'items'     =>  $menu->getItemsList(),
            'title'     =>  'Edit',
            'button'    =>  'Edit',
            'edit'      =>  true,
            'route'     =>  'update.menu.admin',
            'item'      =>  $menu->getItemById($id)

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, MenuRepository $menu)
    {
        $update = $menu->update( $request );
        if ( $update === true ) 
            return redirect(route('menus.admin'));
        else
        {   
            session(['errors' => $update ]);
            return redirect(route('edit.menu.admin', $request->id))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCategories( CategoryRepository $cat )
    {
        return view('admin.layouts.pages.category', [
            'get'           =>  $this->admin,
            'categories'    =>  $cat->getAll()  

        ]);
    }

    public function postCategory(Request $request, CategoryRepository $cat)
    {
        if( $request->has('id') )
        {
            if( $request->has('delete') )
            {
                $id = $request->id;
                $cat->destroy($id);
                return redirect( route('cats.admin') );
            }
            else{
                $update = $cat->update($request);
                if( $update === true )
                    return redirect(route('cats.admin'));
                else
                {
                    session(['errors' => $update]);
                    return redirect(route('cats.admin'));
                }
            }
        }
        else
        {
            $store = $cat->store($request);
            if( $store === true )
                return redirect(route('cats.admin'));
            else
            {
                session(['errors' => $store]);
                return redirect(route('cats.admin'))->withInput();
            }
        }


    }
}

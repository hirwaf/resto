<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id', 'description', 'price', 'special', 'image' 
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}

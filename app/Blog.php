<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
	use SoftDeletes;

	/**
	* The attributes that should be mutated to dates.
	*
	* @var array
	*/

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'excerpt', 'slug', 'content', 'image', 'tags', 'from_net', 'source', 'author', 'published'
    ];
}

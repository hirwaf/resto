<?php
namespace App\Repositories;

use App\Cog;
use Image;
/**
* 
*/
class CogRepository
{
	
	function __construct()
	{
		# code...
	}

	public function siteinfo($name=""){
    	if($name != "" ){
    		if($value = Cog::where('name',$name)->get()->first())
    			return $value->value;
    		else 
    			return "";
    	}
    	return "";
    }

    public function store($request){
    	if( $this->validates($request) === true ){
    		Cog::where('name','name')->update(['value' => $request->name ]);
    		Cog::where('name','email')->update(['value' => $request->email ]);
    		Cog::where('name','booking')->update(['value' => $request->booking ]);
    		Cog::where('name','care')->update(['value' => $request->information ]);
    		Cog::where('name','facebook')->update(['value' => $request->facebook ]);
    		Cog::where('name','pinterest')->update(['value' => $request->pinterest ]);
    		Cog::where('name','instagram')->update(['value' => $request->instagram ]);
    		Cog::where('name','days')->update(['value' => $request->days ]);
    		Cog::where('name','suturday')->update(['value' => $request->saturday ]);
            Cog::where('name','sunday')->update(['value' => $request->sunday ]);
    		Cog::where('name','description')->update(['value' => $request->description ]);
    		
    		if( $request->hasFile('icon') ){
    			$upload = $this->setIcon($request->file('icon'));
	    		if( $upload )
	    			Cog::where('name','icon')->update([ 'value' => $upload ]);
    		}

    		return true;

    	}
    	else return $this->validates($request);
    }

    public function storestyles($request){
        $styles = json_decode($this->siteinfo('styles'),true);
        $styles['top']  =   $request->top;
        $styles['nav']  =   $request->nav;
        $styles['header-size']  =   $request->size;
        $pages = new PageRepository;
        $pages = $pages->getSiteMenu();
        if( count($pages) >= 1 )
        {
            foreach ($pages as $page) {
                if( $page->slug == 'welcome' )
                    continue;
                
                $image = $this->upload($request,$page->slug);
                $styles[$page->slug] = $image != "" ? $image : $styles[$page->slug];
            }
        }

        $styles = json_encode($styles);

        Cog::where('name', 'styles')->update(['value' => $styles ]);

        return true;
    }

    protected function upload($request,$fname)
    {
        if ( $request->hasFile($fname)) {
            $file = $request->file($fname);
            $path = public_path()."/images/web/";
            $ex = explode('.', $file->getClientOriginalName());
            $ex = end($ex);
            $name = $fname.'-'.time().'.'.$ex;

            $image = \Image::make($file);
            $image->resize(1390,720);

            if( $image->save($path.$name) ) return $name;

            else return "";
        }
        else return "";
    }

    public function getStyle($el)
    {
        $styles = json_decode($this->siteinfo('styles'),true);

        return $styles[$el];
    }

    protected function setIcon($file){
    	$ex = explode('.', $file->getClientOriginalName());
    	$ex = end($ex);
    	$name = time().'.'.$ex;
    	$path = public_path()."/images/web/icons/";

    	$image = Image::make($file);
    	$image->resize(86.73,60);

    	if( $image->save($path.$name) ) return $name;
    	
    	else return false;
    }
    	
    protected function validates($request){
    	$validate = \Validator::make($request->all(), $this->rules());	
    	if( $validate->fails() ){
    		return $validate->errors()->all();
    	}
    	else
    		return true;
    }

    protected function rules(){
    	return [
    		'name'			=>	'required|min:3',
    		'email'			=>	'required|email',
    		'booking'		=>	'required|min:14|max:14',
    		'information'	=>	'required|min:14|max:14',
    		'facebook'		=>	'min:25',
    		'pinterest'		=>	'min:26',
    		'instagram'		=>	'min:26',
    		'days'			=>	'required|min:5',
    		'saturday'		=>	'required|min:5',
    		'sunday'		=>	'required|min:5',
    		'description'	=>	'min:10'
    	];	
    }


}
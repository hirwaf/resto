<?php
namespace App\Repositories;

use App\Event;

/**
* 
*/
class EventRepository
{
	
	function __construct()
	{
		
	}

	
	public function getAllEvents()
	{
		$now = date('Y-m-d');
		return Event::orderBy('date','asc')
					->where( 'date','>=',$now )
					->paginate(2);
		
	}

	public function getEventById($id)
	{
		return Event::FindOrFail($id);
	}


	public function store($request)
	{
		$validator = $this->validates($request);
		if ( $validator === true ) {
			$image = [];
			$image['xs'] = $this->upload($request,60,60,'60x60') ? $this->upload($request,60,60,'60x60') : "" ;
			$image['lg'] = $this->upload($request,1600,850,'1600x850') ? $this->upload($request,1600,850,'1600x850') : "" ;
			$image = json_encode($image);
			Event::create([
				'title'		=>	$request->title,
				'summary'	=>	$request->summary,
				'date'		=>	$request->date,
				'start'		=>	$request->start,
				'end'		=>	$request->end,
				'entrence'	=>	$request->entrence,
				'image'		=>	$image,
				'valid'		=>	true
			]);

			return true;
		}
		else 
			return $validator;
	}

	public function update($request)
	{
		$event = Event::find($request->id);
		$tle = $request->title != $event->title ? false : true;
		$img = true;
		$validator = $this->validates($request,$tle,$img);
		if ( $validator === true ) {

			if ( $request->hasFile('image') ) {
				$image = [];
				$image['xs'] = $this->upload($request,60,60,'60x60') ? $this->upload($request,60,60,'60x60') : "" ;
				$image['lg'] = $this->upload($request,1600,850,'1600x850') ? $this->upload($request,1600,850,'1600x850') : "" ;
				$image = json_encode($image);
				$event->image	=	$image;
			}

			$event->title	 =	$request->title;
			$event->summary	 =	$request->summary;
			$event->date	 =	$request->date;
			$event->start	 =	$request->start;
			$event->end		 =	$request->end;
			$event->entrence =	$request->entrence;
			$event->save();

			return true;
		}
		else 
			return $validator;	
	}

	public function destroy($id)
	{
		if( Event::find($id)->delete() )
			return true;
		else 
			return false;
	}

	protected function upload($request,$w,$h,$name='')
	{
    	if ( $request->hasFile('image') ) {
    		$file = $request->file('image');
    		$path = public_path()."/images/web/events/";
    		$ex = explode('.', $file->getClientOriginalName());
	    	$ex = end($ex);
	    	$name = $name == '' ? "event-".time().'.'.$ex : $name."-".time().'.'.$ex;

	    	$image = \Image::make($file);
	    	$image->resize($w,$h);

	    	if( $image->save($path.$name) ) return $name;

	    	else return false;
    	}
    	else return false;
    }

	protected function rules($title = false,$image = false)
	{
		return [
			'title'		=>	'required|min:2' . $title ? "" : "|unique:events,title",
			'date'		=>	'required|date|after:today',
			'start'		=>	'required',
			'end'		=>	'required',
			'entrence'	=>	'required|integer',
			'image'		=>	$image ? "" : "required|" . 'mimes:jpeg,bmp,png',
			'summary'	=>	'required|min:5'
		];
	}

	protected function validates($request,$title=false,$image=false)
	{
		$validator = \Validator::make($request->all(), $this->rules($title,$image));
		if( ! $validator->fails() )
			return true;
		else
			return $validator->errors()->all();
	}
}
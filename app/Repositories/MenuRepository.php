<?php
namespace App\Repositories;

use App\Category;
use App\Menu;

/**
* 
*/
class MenuRepository
{
	protected $rules;

	function __construct()
	{
		$this->rules = [
			'name'			=>	'required|min:2|unique:menus,name',
			'description'	=>	'required|min:5'
		];
	}
	

	public function getItemsList()
	{
		return Category::orderBy('name','asc')->get();
	}

	public function getItemById($id)
	{
		return Menu::findOrFail($id);
	}

	public function store($request)
	{	
		$validator = $this->validates($request);
		if ( $validator === true ) {
			
			$name  = $request->name;
			$cat   = $request->category;
			$price = json_encode($request->input('price'));
			$special = $request->special ? true : false;
			$desc = $request->description;
			$file = $request->file('image');
			$path = public_path()."/images/web/dishs/";
			$image1 = $this->upload($file,60,60,$path,"90x81");
			$image2 = $this->upload($file,263,370,$path,"263x370");

			$image = [ 
				'90x81'		=>	$image1,
				'263x370' 	=>	$image2
			];
			$image = json_encode( $image );

			Menu::create([
				'name'			=>	$name,
				'category_id'	=>	$cat,
				'description'	=>	$desc,
				'price'			=>	$price,
				'special'		=>	$special,
				'image'			=>	$image
			]);

			return true;

		}
		else return $validator;
	}

	public function update($request)
	{
		$menu = Menu::find($request->id);
		$this->rules = [
			'name'			=>	'required|min:2'.$menu->name == $request->name ? "" : "|unique:menus,name",
			'description'	=>	'required|min:5'
		];
		$validator = $this->validates( $request );
		if ( $validator === true ) {
			
			$menu->name  = $request->name;
			$menu->category_id   = $request->category;
			$menu->price = json_encode($request->input('price'));
			$menu->special = $request->special ? true : false;
			$menu->description = $request->description;
			if( $request->hasFile('image') )
			{
				$file = $request->file('image');
				$path = public_path()."/images/web/dishs/";
				$image1 = $this->upload($file,60,60,$path,"90x81");
				$image2 = $this->upload($file,263,370,$path,"263x370");
				
				$image = [ 
					'90x81'		=>	$image1,
					'263x370' 	=>	$image2
				];
				$image = json_encode( $image );

				$menu->image = $image;
			}
			$menu->save();

			return true;
		}
		else return $validator;
	}

	public function destroy($id)
	{

	}

	protected function upload($file,$w,$h,$path,$name = '')
	{
    	$ex = explode('.', $file->getClientOriginalName());
    	$ex = end($ex);
    	$o  = $name == '' ? "": $name."-";
    	$name = $o.time().'.'.$ex;

    	$image = \Image::make($file);
    	$image->resize($w,$h);

    	if( $image->save($path.$name) ) return $name;
    	
    	else return false;
    }

	protected function validates($request)
	{
		$validator = \Validator::make( $request->all(), $this->rules );
		if ( ! $validator->fails() ) 
			return true;
		else 
			return $validator->errors()->all();
	}



}
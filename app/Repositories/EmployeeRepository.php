<?php

namespace App\Repositories;

use App\Employee;

/**
* 
*/

class EmployeeRepository
{
	
	public function getAllEmployees()
	{
		return Employee::orderBy('firstname','asc')
						->get();
	}

	public function getEmployeeById($id)
	{
		return Employee::findOrFail($id);
	}

	public function getHiredEmployees()
	{
		return Employee::onlyTrashed()
						->get();
	}

	public function getEmployeeChefs()
	{
		return Employee::orderBy('firstname','asc')
						->where('chef', true)
						->get();

	}

	public function store($request)
	{
		$validator = $this->validates($request,'nope','nope','nope');
		if ($validator === true) {
			$image = $this->upload($request,400,400);

			$social = json_encode($this->socialNetworks($request));
			Employee::create([
				'firstname'			=>	$request->firstname,
				'lastname'			=>	$request->lastname,
				'gender'			=>	$request->gender,
				'nation_id'			=>	$request->nation_id,
				'telephone'			=>	$request->telephone,
				'email'				=>	$request->email,
				'post'				=>	$request->has('post') ? $request->post : null,
				'chef'				=>	$request->has('chef') ? $request->chef ? true : false : false,
				'note'				=>	$request->has('note') ? $request->note : null,
				'social_networks'	=>	$social,
				'image'				=>	$image
			]);

			return true;
		}
		else
			return $validator;
	}

	public function update($request)
	{
		$employee = Employee::find($request->id);
		
		$id_n = $employee->nation_id == $request->nation_id ? true : 'nope';
		$tel = $employee->telephone == $request->telephone ? true : 'nope';
		$em = $employee->email == $request->email ? true : 'nope';

		$validator = $this->validates($request,$id_n,$tel,$em);
		if ($validator === true) {
			
			$image = $employee->image;
			if( $request->hasFile('image') )
				$image = $this->upload($request,400,400);
			$social = json_encode($this->socialNetworks($request));

			$employee->firstname = $request->firstname;
			$employee->lastname = $request->lastname;
			$employee->gender = $request->gender;
			$employee->nation_id = $request->nation_id;
			$employee->telephone = $request->telephone;
			$employee->email = $request->email;
			$employee->post = $request->post;
			$employee->chef = $request->chef;
			$employee->note = $request->note;
			$employee->social_networks = $social;
			$employee->image = $image;

			$employee->save();

			return true;
		}
		else
			return $validator;
	}

	public function destroy($id,$force='n')
	{
		$employee = Employee::where('id', $id);

		if( $force === 'p' )
			$employee->forceDelete();
		elseif ( $force === 'n' ) 
			$employee->delete();
		elseif ( $force === 'a' )
			$employee->history()->forceDelete();
		else
			return false;

		return true;
	}

	public function restore($id)
	{
		$employee = Employee::where('id',$id);
		$employee->restore();

		return true;
	}

	protected function socialNetworks($request)
	{
		$social = [];
		if ( $request->has('facebook') )
			$social['facebook'] = $request->facebook;
		if ( $request->has('twitter') )
			$social['twitter'] = $request->twitter;
		if ( $request->has('linkedin') )
			$social['linkedin'] = $request->linkedin;

		return $social;
	}

	protected function upload($request,$w,$h,$name='')
	{
		if ( $request->hasFile('image') ) {
			$file = $request->file('image');
    		$path = public_path()."/images/web/employees/";
    		$ex = explode('.', $file->getClientOriginalName());
	    	$ex = end($ex);
	    	$name = $name == '' ? "employee"."-".time().'.'.$ex : $name."-".time().'.'.$ex;

	    	$image = \Image::make($file);
	    	$image->resize($w,$h);
	    	$image->save($path.$name);
	    	
	    	return $name;
		}
		else
			return null;
	}

	protected function rules($id,$tel,$em)
	{
		
		return [
			'firstname'		=>	'required|min:2|max:20|different:lastname',
			'lastname'		=>	'required|min:2|max:20',
			'gender'		=>	'required|min:4|max:6',
			'nation_id'		=>	$id === 'nope' ? "required|min:21|max:21|unique:employees" : 'required|min:21|max:21',
			'telephone'		=>	$tel === 'nope' ? 'required|min:15|max:15|unique:employees' : 'required|min:15|max:15',
			'email'			=>	$em === 'nope' ? 'required|email|unique:employees' : 'required|email',
			'post'			=>	'min:2|string',
			'note'			=>	'min:5|max:200|string',
			'facebook'		=>	'min:5',
			'twitter'		=>	'min:5',
			'linkedin'		=>	'min:5',
			'image'			=>	'image'

		];
	}

	protected function validates($request,$id,$tel,$em)
	{
		$validator = \Validator::make($request->all(), $this->rules($id,$tel,$em));
		if( $validator->fails() )
			return $validator->errors()->all();
		else 
			return true;
	}
}
<?php
namespace App\Repositories;

use App\Page;
use App\Repositories\EventRepository;
use App\Repositories\VideoRepository;
use App\Repositories\BlogRepository;

/**
* Pages Repository
*/

class PageRepository
{
	
	function __construct()
	{
		# code...
	}


	public function getPages()
	{
		return Page::orderBy('position','asc')->get();
	}

	public function getSiteMenu(){
		return Page::orderBy('position','asc')
					->where('enabled', true)
					->get();
	}

	public function changePosition($request){
		$page = Page::findOrFail($request->page);
        $whohas = Page::where('position',$request->position)->first();
        if($whohas){
            $whohas->position = $page->position;
            $whohas->save();
        }
        $page->position = $request->position;
        $page->save();
	}

	public function changeStatus($request){
		$page = Page::findOrFail($request->page);
        $stutas = $page->enabled ? false : true;
        $page->enabled = $stutas;
        $page->save();
	}

	public function getHome()
	{
		return Page::find(1);
	}

	public function getSlides($slug = 'welcome')
	{
		if( $this->getHome()->slug == $slug ){
			$content = \App\Content::where('page_id',$this->getHome()->id)->get()->first();
			$get = [];
			if($content)
				$get = json_decode($content->content,true);
			return $get;

		}else return [];
	}

	public function changeSlides($request)
	{
		$slides = count($this->getSlides());
		$path = public_path()."/images/web/slides/";
		$file = $this->upload($request->file('image'),800,600,$path);
		$caption = $request->caption == NULL ? null : $request->caption;
		if( $request->new == 'New' && $slides <= 0 ){
			$data = [
				'1'	=>	[
					'caption' 	=> 	$caption,
					'image'		=>	$file
				]
			];
		}
		elseif ( $request->new == 'New' && $slides > 0 && $slides < 5 ) {
			$data = $this->getSlides();
			$data[($slides + 1)] = [
				'caption' 	=> 	$caption,
				'image'		=>	$file
			];
		}
		else{
			$replace = $request->new != 'New'? $request->new : 1;
			$data = $this->getSlides();
			$data[$replace] = [
				'caption'	=> 	$caption,
				'image'		=>	$file
			];
		}

		$content = json_encode($data);

		if( \App\Content::where( 'page_id', $this->getHome()->id )->first() != null  )

			\App\Content::where( 'page_id', $this->getHome()->id )->update( [ 'content' => $content ] );
		else
			\App\Content::create( [
				'page_id' => $this->getHome()->id,
				'content' => $content
			] );
		
	} 

	public function getGalleries($category = null)
	{
		if( \App\Gallery::count() > 0 ){

			if($category == null)
				return \App\Gallery::orderBy('created_at', 'desc')->get();
			else
				return \App\Gallery::where('category_id',$category)
									->orderBy('created_at', 'desc')
									->get();

		}

		else return 0;

	}

	public function getAboutUs($slug = 'about-us')
	{
		$about = Page::where('slug',$slug)->get()->first();
		$content = \App\Content::where('page_id', $about->id)->get()->first();
		$get = json_decode($content != null ? $content->content : json_encode([])  , true);
		if( empty( $get ) ){
			$get = [
				'small'		=>	'We are Taste Cuisine',
				'normal'	=>	'Our Mission is to satisfy',
				'text'		=>	'',
				'image'		=>	'feature-img-1.png'
			];
		}
		return $get;
	}

	public function updateAboutUs($request)
	{

		$image = 'feature-img-1.png';
		
		if ( $request->hasFile('image') ) {
			$file = $request->file('image');
			$path = public_path()."/images/web/";
			$image = $this->upload($file,401,286,$path);
		}

		$data = [
			'small'		=>	$request->small,
			'normal'	=>	$request->normal,
			'text'		=>	$request->text,
			'image'		=>	$image
		];
		$exist = \App\Content::where('page_id', '6')->first();
		if( $exist == null ){
			\App\Content::create([
				'page_id'	=>	'6',
				'content'	=>	json_encode($data)
			]);
		}
		else{
			\App\Content::where('page_id','6')->update([ 'content' => json_encode($data) ]);
		}

	}

	public function storeGallery($request)
	{
		if ( $request->hasFile('image') ) 
		{
			$file = $request->file('image');
			$path = public_path()."/images/web/gallery/";
			$category = $request->category;
			$title =  $request->has('title')? $request->title : null;
			$caption = $request->has('caption')? $request->caption : null;
			$image = $this->upload($file,1600,850,$path);
			if( $image !== false )
			{
				\App\Gallery::create([
					'title'			=>	$title,
					'category_id'	=>	$category,
					'url'			=>	$image,
					'caption'		=>	$caption,
					'published'		=>	true
				]);

				return true;
			}
			else return false;
		}
		else return false;
	}

	public function deleteGallery($id)
	{
		\App\Gallery::find($id)->delete();
	}

	public function getSpecialties()
	{
		return \App\Menu::orderBy('created_at','desc')
						->where('special',true)
						->get();
	}

	public function readJson($json)
	{
		return json_decode($json,true);
	}

	public function getEventNear()
	{
		$event = new EventRepository;
		return $event->getAllEvents()->first();
	}

	public function getTimeFromString($string)
	{
		$ex = explode(" ", $string);
		$date = explode('-', $ex[0]);
		
		$carbon = \Carbon::create($date[0],$date[1],$date[2],0);

		return $carbon->toFormattedDateString();
	}

	public function getAllEvents()
	{
		return $event;
	}

	public function getItemsList($n=9)
	{
		$menu = \App\Menu::orderBy('name','asc')
						->paginate($n);

		return $menu;
	}

	public function getAllGalleries()
	{
		$gallery = \App\Gallery::orderBy('created_at','desc')
								->where('published',true)
								->paginate(30);
		return $gallery;
	}

	public function getVideos($n=25)
	{
		$video_repo = new VideoRepository;

		return $video_repo->getAllVideos($n);
	}

	public function getBlogs()
	{
		$blog_repo = new BlogRepository;

		return $blog_repo->getAllBlog(3);
	}

	public function getChefs()
	{
		$emplo_repo = new EmployeeRepository;
		return $emplo_repo->getEmployeeChefs();
	}

	protected function upload($file,$w,$h,$path){
    	$ex = explode('.', $file->getClientOriginalName());
    	$ex = end($ex);
    	$name = time().'.'.$ex;

    	$image = \Image::make($file);
    	$image->resize($w,$h);

    	if( $image->save($path.$name) ) return $name;
    	
    	else return false;
    }

    public function getCategories()
    {
    	return json_decode(\App\Category::orderBy('name','asc')->get(),true);
    }

}
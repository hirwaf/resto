<?php
namespace App\Repositories;

use App\Video;

/**
* 
*/
class VideoRepository
{
	protected $url;
	
	public function getAllVideos($n=null)
	{
		if( $n == null )
		{
			return Video::orderBy('created_at', 'desc')
					->where('valid',true)
					->where('from_net', true)
					->get();
		}
		else
		{
			return Video::orderBy('created_at', 'desc')
					->where('valid',true)
					->where('from_net', true)
					->paginate($n);
		}
	}

	public function getVideoById($id)
	{
		return Video::find($id);
	}

	public function store($request)
	{
		$validator = $this->validates($request);
		if ( $validator === true ) {
			$image = $this->getYoutubeVideoImage($request->url);

			Video::create([
				'title'		=>	$request->title,
				'from_net'	=>	true,
				'net_url'	=>	$request->url,
				'name'		=>	$image,
				'valid'		=>	true
			]);

			return true;
		}
		else 
			return $validator;
	}

	public function update($request)
	{	
		$video = Video::find($request->id);

		$validator = $this->validates($request, $video->title == $request->title ? true : false);
		if ( $validator === true ) {
			
			$video->title =	$request->title;
			$video->save();

			return true;
		}
		else 
			return $validator;
	}

	public function destroy($id)
	{
		return Video::find($id)->delete() ? true : false;
	}

	protected function upload()
	{

	}

	protected function rules($update=false)
	{
		return [
			'title'	=>	'required|min:2'. $update == false ? " | unique:videos,title ": "",
			'url'	=>	'required|url'	
		];
	}
	protected function validates($request,$update=false)
	{
		$validator = \Validator::make($request->all(), $this->rules($update));
		if ( ! $validator->fails() )
			return true;
		else
			return $validator->errors()->all();
	}

	public function getYoutubeVideoImage($youtube_code, $size='lg')
	{
		if ( strpos($youtube_code, "v=") ) {
			$url = explode("v=", $youtube_code);
			$this->url = end($url);
		}
		elseif ( strpos($youtube_code, 'youtu.be') ) {
			$url = explode('/', $youtube_code);
			$this->url = end($url);
		}
		switch ($size) {
			case 'lg':
					$image = "http://img.youtube.com/vi/".$this->url."/0.jpg";
				break;
			case 'md':
					$image = "http://img.youtube.com/vi/".$this->url."/1.jpg";
				break;	
			case 'sm':
					$image = "http://img.youtube.com/vi/".$this->url."/2.jpg";
				break;
			case 'xs':
					$image = "http://img.youtube.com/vi/".$this->url."/3.jpg";
				break;
		}

		return $image;
	}

	public function getVideoViews()
	{
		$video_ID = "7lCDEYXw3mM";
		$jsonURL = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id={$video_ID}&key=YOUR_KEY_HERE&part=statistics");
		$json = json_decode($jsonURL);
		$views = $json->{'items'}[0]->{'statistics'}->{'viewCount'};
		
		return number_format($views,0,'.',',');
	}

}
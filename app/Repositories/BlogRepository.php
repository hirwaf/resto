<?php
namespace App\Repositories;

use App\Blog;

/**
* 
*/
class BlogRepository
{

	public function getBlogById($id)
	{
		return Blog::findOrFail($id);			
	}

	public function getAllBlog($n=null)
	{
		if ( $n == null ) {
			return Blog::orderBy('created_at', 'desc')
					->where('published',true)
					->get();
		}
		else{
			return Blog::orderBy('created_at', 'desc')
					->where('published',true)
					->paginate($n);
		}
	}

	public function getDraftedBlog()
	{
		return Blog::orderBy('created_at', 'desc')
					->where('published', false)
					->get();
	}

	public function getArchieves()
	{
		return Blog::onlyTrashed()->get();
	}

	public function store($request)
	{
		$validator = $this->validates($request,'nope');
		if( $validator === true )
		{	
			$image = $this->upload($request,1161,466,'blog');
			$image = $image === false ? " " : $image;

			Blog::create([
				'title'		=>	$request->title,
				'excerpt'	=>	$request->excerpt,
				'slug'		=>	$request->slug,
				'content'	=>	$request->content,
				'image'		=>	$image,
				'tags'		=>	$request->tags ? $request->tags : null,
				'from_net'	=>	$request->from_net ? true : false,
				'source'	=>	$request->from_net ? $request->source : null,
				'author'	=>	$request->author,
				'published'	=>	$request->published ? false : true
			]);

			return true;
		}
		else
			return $validator;
	}

	public function update($request)
	{
		$blog = Blog::find($request->id);
		$validator = $this->validates($request, $blog->title == $request->title ? true : 'nope' );
		if ($validator === true) 
		{
			if( $request->hasFile('image') )
			{
				$image = $this->upload($request,1161,466,'blog');
				$image = $image === false ? " " : $image;
				$blog->image = $image;
			}
			$blog->title     = $request->title;
			$blog->excerpt   = $request->excerpt;
			$blog->slug      = $request->slug;
			$blog->content   = $request->content;
			$blog->tags      = $request->tags;
			$blog->from_net	 = $request->from_net;
			$blog->source    = $request->from_net ? $request->source : null;
			$blog->author    = $request->author;
			$blog->published = $request->published ? false : true;
			$blog->save();
			
			return true;
		}
		else
			return $validator;
	}

	public function destroy($id,$soft=true)
	{
		if($soft === true)
			Blog::find($id)->delete();
		else
			Blog::find($id)->forceDelete();

		return true;
	}

	protected function upload($request,$w,$h,$name='')
	{
    	if ( $request->hasFile('image') ) 
    	{
    		$file = $request->file('image');
    		$ex = explode('.', $file->getClientOriginalName());
	    	$ex = end($ex);
	    	$name = $name == '' ? "blog-".time().'.'.$ex : $name."-".time().'.'.$ex;

    		$path = public_path()."/images/web/blogs/";
	    	$image = \Image::make($file);
	    	$image->resize($w,$h);

	    	if( $image->save($path.$name) ) return $name;
	    	else return false;
    	}
    	else return false;
    }

	protected function rules($update)
	{
		return [
			'title'		=>	'required|min:2'. $update == 'nope' ? "|unique:blogs,title ": "|unique:blogs,title",
			'author'	=>	'required|min:2',
			'excerpt'	=>	'required|min:10|max:255'. $update == 'nope' ? "|unique:blogs,excerpt" : "",
			'content'	=>	'required|min:20',
			'source'	=>	'required_if:from_net,'.true. $update == 'nope' ? "|unique:blogs,source" : "",
			'image'		=>	$update == 'nope' ? "required|" : "" .'mimes:jpeg,bmp,png',
		];
	}

	protected function validates($request,$update)
	{
		$validator  = \Validator::make($request->all(), $this->rules($update));
		if( $validator->fails() )
			return $validator->errors()->all();
		else
			return true;
	}

}
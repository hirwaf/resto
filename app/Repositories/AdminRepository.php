<?php 
namespace App\Repositories;

use App\Admin;
use Auth;

class AdminRepository
{
	protected $admin;
 	
	public function __construct()
	{
		$this->admin = new Admin;
		if( \Auth::guard('admins')->check() )
			$this->admin = Auth::guard('admins')->user();
	}

	public function admin($value='',$id = null)
	{
		if($value != ""){
			$admin = Auth::guard('admins')->user();
			return $admin[$value];
		}
		elseif ( $value != "" && is_integer($id) ) {
			
		}
		else
			return "";
	}

	public function elix(){
		return $this->admin->isHe();
	}
	
	public function update($request)
	{
		
		if ( ! \Hash::check( $request->currentpassword, $this->admin('password') ) ) {
			return ["Incorrect Current Password"];
		}
		elseif ( ! $this->validates($request)->fails() ) 
		{
			$this->admin->name = $request->name;
			$this->admin->username = $request->username;
			$this->admin->email = $request->email;
			$this->admin->telephone = $request->telephone;
			
			if($request->password != "" || $request->password != null)
				$this->admin->password = bcrypt($request->password);
			
			$this->admin->save();

			return true;

		}
		else{
			return $this->validates($request)->errors()->all();
		}
	}

	public function getMessages()
	{
		return \App\Message::orderBy('created_at', 'desc')
							->paginate(10);
	}

	public function getCountNewMessages()
	{
		return \App\Message::orderBy('created_at', 'desc')
							->where('read', false)
							->where('seen', false)
							->count();
	}

	public function makeSeen($id)
	{
		return \App\Message::where('id',$id)->update(['seen' => true]);
	}

	public function makeAsRead($id)
	{
		return \App\Message::where('id',$id)->update(['read' => true]);
	}

	protected function rules()
	{
		return [
			'name' 		=> 	'required|min:3'
		];
	}
	protected function validates($request)
	{
		$rules = $this->rules();
		$rules['email'] = "required|email".($request->email == $this->admin('email')? "" : "|unique:admins,email");
		$rules['username'] = "required|min:2".($request->username == $this->admin('username')? "" : "|unique:admins,username");
		$rules['telephone'] = "required|integer".($request->telephone == $this->admin('telephone')? "" : "|unique:admins,telephone");
		if($request->password != "" || $request->password != null)
			$rules['password'] = "required|min:6|confirmed";
		
		return \Validator::make($request->all(),$rules);
	}


}

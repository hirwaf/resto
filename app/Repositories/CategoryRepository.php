<?php
namespace App\Repositories;

use App\Category;

/**
* 
*/
class CategoryRepository
{
	
	function __construct()
	{
		# code...
	}


	public function getAll()
	{
		return Category::orderBy('updated_at','desc')->get();
	}

	public function getById($id)
	{

	}

	public function store($request)
	{
		$validator = $this->validators($request);
		
		if( $validator === true )
		{
			Category::create([
				'name'	=>	$request->categoryName,
				'is'	=>	$request->is,
				'valid'	=>	true
			]);

			return true;
		}

		else return $validator;
	}

	public function update($request)
	{
		$validator = $this->validators($request,$request->categoryName);
		
		if( $validator === true )
		{
			$category = Category::find($request->id);
			$category->name =	$request->categoryName;
			$category->is	=	$request->is;
			$category->save();
			return true;
		}

		else return $validator;
	}

	public function destroy($id)
	{
		Category::find($id)->delete();	
	}

	protected function rules( $update = false )
	{
		$cond = $update == false ? "|unique:categories,name" : "";
		return [
			'categoryName'	=>	'required|min:2'.$cond,
		];
	}

	protected function validators($request, $update = false)
	{
		$validator = \Validator::make( $request->all(),$this->rules( $update == false ? false : $request->name ) );
		return $validator->fails() ? $validator->errors()->all()  : true  ;
	}


}
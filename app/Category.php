<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'is', 'valid'
    ];

    public function menu()
    {
    	return $this->hasMany('App\Menu');
    }

    public function gallery()
    {
    	return $this->hasMany('App\Gallery');
    }

}

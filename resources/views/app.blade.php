
<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php \Astroanu\SEOGen\Renderer::render($metaData); ?>
		<title>@yield('title')</title>
		<link rel="icon" href="{{ asset(url('images/web/icons/'.$info->icon )) }}">
		<!-- Bootstrap -->
		<link href="{{ asset(url('css/bootstrap.min.css')) }}" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="{{ asset(url('fonts/font-awesome/css/font-awesome.min.css')) }}" rel="stylesheet">
		<!-- Simple-Line-Icons-Webfont -->
		<link href="{{ asset(url('fonts/Simple-Line-Icons-Webfont/simple-line-icons.css')) }}" rel="stylesheet">
		<!-- FlexSlider -->
		<link href="{{ asset(url('scripts/FlexSlider/flexslider.css')) }}" rel="stylesheet">
		<!-- Owl Carousel -->
		<link href="{{ asset(url('css/owl.carousel.css')) }}" rel="stylesheet">
		<link href="{{ asset(url('css/owl.theme.default.css')) }}" rel="stylesheet">
		<!-- Nivo Lightbox -->
		<link href="{{ asset(url('scripts/Nivo-Lightbox/nivo-lightbox.css')) }}" rel="stylesheet">
		<link href="{{ asset(url('scripts/Nivo-Lightbox/themes/default/default.css')) }}" rel="stylesheet">
		<!-- Style.css -->
		<link href="{{ asset(url('css/style.css')) }}" rel="stylesheet">
		@include('style')
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>

		<div class="responsive-menu">
			<a href="" class="responsive-menu-close"><i class="icon-close"></i></a>
			<nav class="responsive-nav"></nav> <!-- end .responsive-nav -->
		</div> <!-- end .responsive-menu -->
		<header class="header transparent">
			<div class="top">
				<div class="container">
					<div class="left">
						<div class="row">
							<div class="col-md-3 visible-sm visible-md visible-lg">
								<div class="logo">
									<img src="{{ asset(url('/images/web/icons/'.$info->icon)) }}" alt="{{ $info->name }}" class="img-responsive">
								</div>
							</div>
							<div class="col-md-9">
								<span>Welcome to {{ $info->name }}</span>
							</div>
						</div>
					</div> <!-- end .left -->
					<div class="right">
						<span class="item"><a href="tel:{{ $info->care }}">{{ $info->care }}</a></span>
						<span class="item"><a href="mailto:{{ $info->email }}">{{ $info->email }}</a></span>
					</div> <!-- end .right -->
				</div> <!-- end .container -->
			</div> <!-- end .top -->
			<div class="navigation">
				<div class="container clearfix">
					
					@yield('main-nav')
					<!-- end .main-nav -->
					<a href="" class="responsive-menu-open"><i class="fa fa-bars"></i></a>
				</div> <!-- end .container -->
			</div> <!-- end .navigation -->
		</header> <!-- end .header -->

		@yield('content')

		<footer class="footer" style="color: #fff;">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<h6 style="color: #ddd;">Address</h6>
						<p>{{ $info->name }}</p>
						<p>Rwanda Kigali</p>
						<p>10300 Venus, Milky Way</p>
					</div> <!-- end .col-sm-3 -->
					<div class="col-sm-3">
						<h6 style="color: #ddd;">Social Media</h6>
						{{ $info->facebook != ""? ' <p><a href="'.$info->facebook.'">Facebook</a></p> ' : ''  }}
						{{ $info->instagram != ""? ' <p><a href="'.$info->instagram.'">Instagram</a></p>  ' : ''  }}
						{{ $info->pinterest != ""? ' <p><a href="'.$info->pinterest.'">Pinterest</a></p> ' : ''  }}
					</div> <!-- end .col-sm-3 -->
					<div class="col-sm-3">
						<h6 style="color: #ddd;">Open Hours</h6>
						<div class="row">
							<div class="col-xs-6">
								<p>Monday - Friday</p>
								<p>Saturdays</p>
								<p>Sundays</p>
							</div> <!-- end .col-xs-6 -->
							<div class="col-xs-6">
								<p><b> {{ $info->day }} </b></p>
								<p><b> {{ $info->suturday }} </b></p>
								<p><b> {{ $info->sunday }} </b></p>
							</div> <!-- end .col-xs-6 -->
						</div> <!-- end .row -->
					</div> <!-- end .col-sm-3 -->
					<div class="col-sm-3">
						<h6 style="color: #ddd;">Contact</h6>
						<p><a href="mailto:{{ $info->email }}">{{ $info->email }}</a></p>
						<div class="row">
							<div class="col-xs-6">
								<p>Booking</p>
								<p>Information</p>
							</div> <!-- end .col-xs-6 -->
							<div class="col-xs-6">
								<p><b>{{ $info->booking }}</b></p>
								<p><b>{{ $info->care }}</b></p>
							</div> <!-- end .col-xs-6 -->
						</div> <!-- end .row -->
					</div> <!-- end .col-sm-3 -->
				</div> <!-- end .row -->
			</div> <!-- end .container -->
		</footer> <!-- end .footer -->
		<div class="container-flat" style="background: #ECF0F1;">
			<div class="row">
				<div class="col-md-12 text-center dev-footer">
					<span>Designed & Developed by <a href="http://www.elix.2hf.co">hirwaf</a></span>
				</div>
			</div> <!-- end .row -->
		</div>
		<style type="text/css">
			body{
				overflow-x: hidden;
			}
		</style>
		<!-- jQuery -->
		<script src="{{ asset(url('js/jquery.min.js')) }}"></script>
		<!-- Bootstrap -->
		<script src="{{ asset(url('js/bootstrap.min.js')) }}"></script>
		<!-- google maps -->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
		<!-- FlexSlider -->
		<script src="{{ asset(url('scripts/FlexSlider/jquery.flexslider-min.js')) }}"></script>
		<!-- Owl Carousel -->
		<script src="{{ asset(url('js/owl.carousel.min.js')) }}"></script>
		<!-- Nivo Lightbox -->
		<script src="{{ asset(url('scripts/Nivo-Lightbox/nivo-lightbox.min.js')) }}"></script>
		<!-- Isotope -->
		<script src="{{ asset(url('js/isotope.pkgd.min.js')) }}"></script>
		<script src="{{ asset(url('js/imagesloaded.pkgd.min.js')) }}"></script>
		<!-- Scripts.js -->
		<script src="{{ asset(url('js/scripts.js')) }}"></script>
		@yield('scripts')
	</body>
</html>

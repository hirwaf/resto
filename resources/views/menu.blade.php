@extends('app')

@section('title',"Menu")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="section dark small transparent parallax page-title" style="background-image: url({{ asset(url('images/web/'.$bg))  }});">
<div class="inner">
<div class="container">
	<div class="row aligned-cols">
		<div class="col-sm-3 aligned-middle">
			<h2>Our Foods</h2>
			</div> <!-- end .col-sm-3 -->
			<div class="col-sm-9">
				<p>{{ $info->description }}</p>
			</div> <!-- end .col-sm-9 -->
		</div> <!-- end .row -->
	</div> <!-- end .container -->
</div> <!-- end .inner -->
</div> <!-- end .page-title -->

	<div class="section white">
		<div class="inner">
			<div class="container">
				<div class="row">
					@if( count( $page->getItemsList() ) > 1 )
						@foreach( $page->getItemsList() as $item  )
						<div class="col-sm-4">
							<div class="menu clearfix">
								<img src="{{ asset(url('images/web/dishs/'.$page->readJson( $item->image )['90x81'] )) }}" alt="alt text">
								<div class="content">
									<span class="price">{{ $page->readJson( $item->price )['r'] }} RWF</span>
									<h5 style="text-transform: capitalize;">{{ ucfirst( $item->name ) }}<span class="label green"></span></h5>
									<p>{{ substr( $item->description,0,90 ) }}</p>
									<!-- <button role='button' class="btn btn-sm btn-block btn-more" data-div="div-{{ $item->id }}" >More</button> -->
								</div> <!-- end .menu -->
							</div> <!-- end .menu -->
							<div class="col-sm-12 div-{{ $item->id }} hidden"  >
								<h2>{{ $item->name }}</h2>
							</div>
						</div> <!-- end .col-sm-4 -->
						@endforeach
					@endif
			</div> <!-- end .row -->
			<div class="row">
				<nav>
					<ul class="pager">
						<li class="previous"><a href="{{ $page->getItemsList()->previousPageUrl() }}">Last Page</a></li>
						<li class="next"><a href="{{ $page->getItemsList()->nextPageUrl() }}">Next Page</a></li>
						<li class="page-number"> {{ $page->getItemsList()->currentPage() }} </li>
					</ul>
				</nav>
			</div>
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function(){
			$("*.btn-more").on('click',function(){
				var v = $(this).attr('data-div');
				$("."+v).removeClass("hidden");
			});
		});
	</script>
@endsection
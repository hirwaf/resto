@extends('app')

@section('title',"Events")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="section dark small page-title transparent parallax" style="background-image: url('{{ asset(url('images/web/'.$bg))  }}');">
	<div class="inner">
		<div class="container">
			<div class="row aligned-cols">
				<div class="col-sm-3 aligned-middle">
					<h2>Have fun with Us</h2>
				</div> <!-- end .col-sm-3 -->
				<div class="col-sm-9">
				<p>
					{{ $info->description }}
				</p>
				</div> <!-- end .col-sm-9 -->
			</div> <!-- end .row -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .page-title -->

<div class="section white">
	<div class="inner">
		<div class="container">
			@if( count($page->getEventNear()) >= 1 )
				<div class="event">
					<div class="image-wrapper">
						<img src="{{ asset(url('images/web/events/'.json_decode($page->getEventNear()->image,true)['xs'] )) }}" alt="alt text" class="img-responsive">
					</div> <!-- end .image-wrapper -->
					<div class="content">
						<h5>{{ $page->getEventNear()->title }}</h5>
						<p>{{ $page->getEventNear()->summary }}</p>
						<ul class="list-inline event-meta">
							<li>Event Date: <span>{{ $page->getTimeFromString($page->getEventNear()->date) }}</span></li>
							<li>Event Duration: <span>{{ $page->getEventNear()->start }} - {{ $page->getEventNear()->end }}</span></li>
							<li>Location: <span> {{ $info->name }} Place  </span></li>
						</ul>
					</div> <!-- end .content -->
				</div> <!-- end .event -->
			@endif
			@if( count( $page->getVideos() ) >= 1 )
			<div class="row">
				<div class="col-md-12">
					<h3>Last Events <small>Videos</small> </h3>
				</div>
				<div class="col-md-12">
					<div class="row">
					@foreach( $page->getVideos() as $video )
						<div class="col-xs-6 col-md-2">
					    	<a href="{{ url(route('view.video.web',$video->id)) }}" class="thumbnail">
					      		<img src="{{ url($video->name) }}" alt="{{ ucfirst($video->title) }}" class="img-responsive" >
					    	</a>
					    	<div class="caption">
					    		<h6 style="text-transform: capitalize;" class="text-muted" >{{ $video->title }}</h6>					    		
					    	</div>
					  	</div>
					@endforeach
					</div>
					{!! $page->getVideos()->links() !!}
				</div>
			</div>
			@endif
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->

@endsection
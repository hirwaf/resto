@extends('app')

@section('title',"Videos")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="page-title section dark small transparent parallax" style="background-image: url('{{ asset(url('images/web/'.$bg))  }}  }}');">
	<div class="inner">
		<div class="container">
			<div class="row aligned-cols">
				<div class="col-sm-3 aligned-middle">
					<h2>Videos</h2>
				</div> <!-- end .col-sm-3 -->
				<div class="col-sm-9">
					<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam uptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt.</p>
				</div> <!-- end .col-sm-9 -->
			</div> <!-- end .row -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .page-title -->
@endsection
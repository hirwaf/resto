@extends('app')

@section('title',"Welcome")

@section('main-nav')
    @include('main_nav')
@endsection


@section('content')

<div id="welcome-slider" class="welcome-slider flex-slider">
    <div class="slides clearfix">
        @if( count($page->getSlides()) >= 1 )
        @foreach( $page->getSlides() as $slide )
            <div class="slide" style="background-image: url('{{ asset(url('images/web/slides/'.$slide['image'])) }}');">
                <div class="inner">
                    <div class="container">
                        <div class="row aligned-cols">
                            <div class="col-sm-6 aligned-middle">
                                <h1>{{ $slide['caption'] }}</h1>
                            </div> <!-- end .col-sm-6 -->
                        </div> <!-- end .row -->
                    </div> <!-- end .container -->
                </div> <!-- end .inner -->
            </div> <!-- end .slide -->
        @endforeach
        @endif
    </div> <!-- end .slides -->
</div> <!-- end .welcome-slider -->
<div class="row">
    <div class="col-sm-12">
        
    </div>
</div>
@if( count( $page->getVideos() ) >= 1 )
    <div class="section white ">
        <div class="inner">
            <div class="container">
                <div class="row">
                    @foreach( $page->getVideos(12) as $video )
                        <div class="col-xs-6 col-md-2">
                            <a href="{{ url(route('view.video.web',$video->id)) }}" class="thumbnail">
                                <img src="{{ url($video->name) }}" alt="{{ ucfirst($video->title) }}" class="img-responsive" >
                            </a>
                            <div class="caption">
                                <h6 style="text-transform: capitalize;" class="text-muted" >{{ $video->title }}</h6>                                
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
@if( count($page->getSpecialties()) >= 1 )
<div class="section white ">
    <div class="transparent parallax partial-background " style="background-image: url('images/web/{{ $bg }}');"></div>
    <div class="inner">
        <div class="container">
            <h2 class="text-center white">Specialties<small>From Our Chefs</small></h2>
            <div class="specialties-slider">
            @foreach( $page->getSpecialties() as $spacialty )
                <div class="specialty">
                    <div class="image">
                        <img src="{{ asset(url('images/web/dishs/'.$page->readJson($spacialty->image)['263x370'] )) }}" alt="alt text here" class="img-responsive">
                        <div class="overlay"><a href="" class="button white">See More</a></div>
                    </div> <!-- end .image -->
                    <h5>{{ ucfirst($spacialty->name) }}</h5>
                    <p>{{ ucfirst( substr($spacialty->description,0,100) )." ..." }}</p>
                </div> <!-- end .specialty -->
            @endforeach    
            </div> <!-- end .specialty-slider -->
        </div> <!-- end .container -->
    </div> <!-- end .inner -->
</div> <!-- end .section -->
@endif
@if( count($page->getEventNear()) >= 1 )
<div class="section medium dark transparent parallax" style="background-image: url({{ asset(url('images/web/events/'.json_decode($page->getEventNear()->image,true)['lg'] )) }});">
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="image-wrapper-1">
                        <img src="{{ asset(url('images/web/events/'.json_decode($page->getEventNear()->image,true)['lg'] )) }}" alt="alt text" class="img-responsive">
                    </div> <!-- end .image-wrapper -->
                </div>
                <div class="col-sm-8 simple-white" style="height: 14.6em; ">
                    <div class="content">
                        <h5>Venus Charity Marathon<span class="label">Price : {{ $page->getEventNear()->entrence ? "Drink" : $page->getEventNear()->entrence == 0 ? "Free" : $page->getEventNear()->entrence  }} </span></h5>
                        <p>Ut convallis pretium diam vel tempus. Phasellus aliquet dui a enim dictum condimentum. Donec quam nunc, placerat vel tempor sed, placerat id augue. Proin erat orci, finibus ut arcu sed, elementum malesuada magna. Vivamus luctus suscipit nunc.</p>
                        <ul class="list-inline event-meta pull-right" style="color: #ddd;">
                            <li>Event Date: <span>{{ $page->getTimeFromString($page->getEventNear()->date) }}</span></li>
                            <li>Event Duration: <span>{{ $page->getEventNear()->start }} - {{ $page->getEventNear()->end }}</span></li>
                        </ul>
                    </div> <!-- end .content --><!-- 
                    <div class="button-wrapper">
                        <a href="" class="button">Join Us</a>
                    </div> <!-- end .button-wrapper -->
                </div> <!-- end .col-sm-6 -->
            </div> <!-- end .row -->
        </div> <!-- end .container -->
    </div> <!-- end .inner -->
</div> <!-- end .section -->
@endif
<div class="section white">
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2><small>{{ $page->getAboutUs()['small'] }}</small>{{ $page->getAboutUs()['normal'] }}</h2>
                    <p>
                        {{ $page->getAboutUs()['text'] }}
                    </p>
                </div> <!-- end .col-sm-6 -->
                <div class="col-sm-6">
                    <img src="{{ asset(url('images/web/'.$page->getAboutUs()['image'])) }}" alt="alt text here" class="img-responsive img-rounded">
                </div> <!-- end .col-sm-6 -->
            </div> <!-- end .row -->
        </div> <!-- end .container -->
    </div> <!-- end .inner -->
</div> <!-- end .section -->
<style type="text/css">
    .specialty img{
        height: 370px;
    }
    .event-meta li span{
        color: #fff !important;
    }
    .welcome .image-wrapper-1 img {
        height: 400px;
    }
</style>
@endsection

<!-- <div class="col-sm-6 text-right">
<div class="hours-box">
<div class="title">Open Hours<span class="open">Open</span></div>
<div class="clearfix"><span class="day">Monday</span><span class="hours">{{ $info->day }}</span></div>
<div class="clearfix"><span class="day">Tuesday</span><span class="hours">{{ $info->day }}</span></div>
<div class="clearfix"><span class="day">Wednesday</span><span class="hours">{{ $info->day }}</span></div>
<div class="clearfix"><span class="day">THursday</span><span class="hours">{{ $info->day }}</span></div>
<div class="clearfix"><span class="day">Friday</span><span class="hours">{{ $info->day }}</span></div>
<div class="clearfix"><span class="day">Saturday</span><span class="hours">{{ $info->suturday }}</span></div>
<div class="clearfix"><span class="day">Sundays</span><span class="hours">{{ $info->sunday }}</span></div>
<div class="info">Info: {{ $info->care }}</div>
</div> 
</div> -->
@extends('app')

@section('title',"Contact-Us")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="section dark transparent parallax" style="background-image: url('{{ asset(url('images/web/'.$bg))  }}');">
	<div class="inner">
		<div class="container">
			<div class="contact-wrapper">
				
				<div class="row">
					<div class="col-sm-6">
						<form action="{{ url('/message/send') }}" method="post" id="contact-form">
							{!! csrf_field() !!}
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" id="contact-name" name="contact-name" placeholder="Your Name" required />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="email" id="contact-email" name="contact-email" placeholder="Email" required />
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<textarea name="contact-message" id="contact-message" placeholder="Your Message" required rows="3"></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="button solid brown">Send</button>
							</div>
							<div id="contact-loading" class="alert alert-info form-alert">
								<span class="icon"><i class="fa fa-info"></i></span>
								<span class="message">Loading...</span>
							</div>
							<div id="contact-success" class="alert alert-success form-alert">
								<span class="icon"><i class="fa fa-check"></i></span>
								<span class="message">Success!</span>
							</div>
							<div id="contact-error" class="alert alert-danger form-alert">
								<span class="icon"><i class="fa fa-times"></i></span>
								<span class="message">Error!</span>
							</div>
						</form>
					</div> <!-- end .col-sm-6 -->
					<div class="col-sm-2 col-sm-offset-1">
						<h6>Address</h6>
						<p>{{ $info->name }}</p>
						<p>Rwanda Kigali</p>
						<p></p>
					</div> <!-- end .col-sm-2 -->
					<div class="col-sm-3">
						<h6>Contact</h6>
						<p><a href="mailto:{{ $info->email }}">{{ $info->email }}</a></p>
						<div class="row">
							<div class="col-xs-6">
								<p>Booking</p>
								<p>Information</p>
							</div> <!-- end .col-xs-6 -->
							<div class="col-xs-6">
								<p><b>{{ $info->booking }}</b></p>
								<p><b>{{ $info->care }}</b></p>
							</div> <!-- end .col-xs-6 -->
						</div> <!-- end .row -->
					</div> <!-- end .col-sm-3 -->
				</div> <!-- end .row -->
				<!-- <div id="map" class="map"></div> -->
			</div> <!-- end .contact-wrapper -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->
<style type="text/css">
	body{
		overflow-x: hidden;
	}
</style>
@endsection
@section('scripts')
<!-- Validator -->
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>
@endsection
@extends('app')

@section('title',"About-Us")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="page-title section dark small transparent parallax" style="background-image: url('{{ asset(url('images/web/'.$bg))  }}');">
	<div class="inner">
		<div class="container">
			<div class="row aligned-cols">
				<div class="col-sm-3 aligned-middle">
					<h2>About Us</h2>
				</div> <!-- end .col-sm-3 -->
				<div class="col-sm-9">
					<p>{{ $info->description }}</p>
				</div> <!-- end .col-sm-9 -->
			</div> <!-- end .row -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .page-title -->
@if( count( $page->getChefs() ) >= 5 )
<div class="section white">
	<div class="inner">
		<div class="container">
			<div class="row">
				@foreach( $page->getChefs() as $chef )
					<div class="col-sm-3">
						<div class="cook">
							<div class="image">
								<img src="{{ asset(url('images/web/employees/'.$chef->image)) }}" alt="{{ $chef->firstname }}" class="img-responsive">
								<!-- <div class="overlay"><a href="" class="button white">Full Profile</a></div> -->
							</div> <!-- end .image -->
							<h5>{{ ucfirst($chef->firstname)." ".ucfirst($chef->lastname) }}</h5>
							<p>{{ ucfirst($chef->note) }}</p>
						</div> <!-- end .cook -->
					</div> <!-- end .col-sm-3 -->
				@endforeach
			</div> <!-- end .row -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->
@endif
<div class="section white border-top">
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2><small>{{ $page->getAboutUs()['small'] }}</small>{{ $page->getAboutUs()['normal'] }}</h2>
                    <p>
                    	{{ $page->getAboutUs()['text'] }}
                    </p>
                </div> <!-- end .col-sm-6 -->
                <div class="col-sm-6">
                    <img src="{{ asset(url('images/web/'.$page->getAboutUs()['image'])) }}" alt="{{ $info->name }}" class="img-responsive img-rounded">
                </div> <!-- end .col-sm-6 -->
            </div> <!-- end .row -->
        </div> <!-- end .container -->
    </div> <!-- end .inner -->
</div> <!-- end .section -->
@endsection
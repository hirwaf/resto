<nav class="main-nav">
	<ul class="list-unstyled">
		@if( count($menus) > 1 )
			@foreach( $menus as $menu )
				<li><a href="{{ url($menu->slug) }}">{{ ucfirst($menu->name) }}</a></li>
			@endforeach
		@endif
	</ul>
</nav>
<a href="" class="responsive-menu-open" style="background-color: #fff;"><i style="color: #000;" class="fa fa-bars"></i></a>
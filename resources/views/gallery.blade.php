@extends('app')

@section('title',"Galleries")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="page-title section small dark transparent parallax" style="background-image: url('{{ asset(url('images/web/'.$bg))  }}');">
	<div class="inner">
		<div class="container">
			<div class="row aligned-cols">
				<div class="col-sm-3 aligned-middle">
					<h2>Galleries</h2>
				</div> <!-- end .col-sm-3 -->
				<div class="col-sm-9">
					<p> {{ $info->description }} </p>
				</div> <!-- end .col-sm-9 -->
			</div> <!-- end .row -->
		</div> <!-- end .container -->		
	</div> <!-- end .inner -->
</div> <!-- end .page-title -->
<div class="section no-padding">
	<div class="inner">
		<div id="gallery" class="gallery">
			<div class="gallery-sizer"></div>
			@if( count( $page->getAllGalleries() ) > 1 )
				@foreach( $page->getAllGalleries() as $gallery )
					<div class="item">
						<img src="{{ asset(url('images/web/gallery/'.$gallery->url)) }}" alt="alt text" class="img-responsive">
						<div class="overlay"><a href="{{ asset(url('images/web/gallery/'.$gallery->url)) }}" class="button white" data-lightbox-gallery="gallery1">View</a></div>
					</div> <!-- end .item -->
				@endforeach
			@endif
		</div> <!-- end .gallery -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->
<div class="section small white">
	<div class="inner">
		<div class="container">
		<nav>
			<ul class="pager">
				<li class="previous"><a href="{{ $page->getAllGalleries()->previousPageUrl() }}">Last Page</a></li>
				<li class="next"><a href="{{ $page->getAllGalleries()->nextPageUrl() }}">Next Page</a></li>
				<li class="page-number">{{ $page->getAllGalleries()->currentPage() }}</li>
			</ul>
		</nav>
	</div>
	</div>
</div>
@endsection
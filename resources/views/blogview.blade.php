@extends('app')

@section('title',"Menu")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
<div class="section dark small transparent parallaxpage-title" style="background-image: url({{ asset(url('images/web/'.$bg)) }});">
	<div class="inner">
		<div class="container">
			<div class="row aligned-cols">
				<div class="col-sm-3 aligned-middle">
					<h2>Blog Talk</h2>
				</div> <!-- end .col-sm-3 -->
				<div class="col-sm-9">
					<p>{{ $info->description }}</p>
				</div> <!-- end .col-sm-9 -->
			</div> <!-- end .row -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .page-title -->
<div class="section white">
	<div class="inner">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="blog-post">
						<a href="{{ url(route('view.blog.web',$blog->slug)) }}"><img src="{{  asset(url('images/web/blogs/'.$blog->image))  }}" alt="{{ $blog->title }}" class="featured-img img-responsive"></a>
						<h2 class="blog-post-title"><a href="" style="text-transform: capitalize;">{{ $blog->title }}</a></a></h2>
						<ul class="blog-post-meta list-inline">
							<li><i class="icon-clock"></i>{{ $blog->created_at->diffForHumans() }}</li>
							<li style="text-transform: uppercase;"><a href=""><i class="icon-user"></i>{{ $blog->author }}</a></li>
							<!-- <li><a href=""><i class="icon-bubble"></i>3 Comments</a></li> -->
						</ul>
						<div style="text-align: justify;">
							{!! $blog->content !!}
						</div>
					</div> <!-- end .blog-post -->
				</div> <!-- end .col-sm-9 -->
				<div class="col-sm-3">
					@if( count( $page->getVideos() ) >= 1 )
					<div class="sidebar-widget">
						<h6>Previous Videos</h6>
					@foreach( $page->getVideos(4) as $video )
						<div class="sidebar-product clearfix">
							<a href="">
								<img src="{{ url($video->name) }}" alt="{{ ucfirst($video->title) }}" width="60" height="60" >
								<div class="content">
									<h5>{{ ucfirst($video->title) }}</h5>
								</div> <!-- end .content -->
							</a>
						</div> <!-- end .sidebar-product -->
					@endforeach
					</div> <!-- end .sidebar-widget -->
					@endif
					@if( count( $page->getSpecialties() ) >= 1 )
					<div class="sidebar-widget">
						<h6>Our Best Menu</h6>
						@foreach( $page->getSpecialties() as $video )
						<div class="sidebar-product clearfix">
							<img src="{{ asset(url('images/web/dishs/'.$page->readJson($video->image)['90x81'] )) }}" alt="{{ $video->name }}" width="60" height="60" >
							<div class="content">
								<h5><a href="" style="text-transform: capitalize;">{{ $video->name }}<span class="price">{{ $page->readJson($video->price)['r'] }} RWF</span></a></h5>
							</div> <!-- end .content -->
						</div> <!-- end .sidebar-product -->
						@endforeach
					</div> <!-- end .sidebar-widget -->
					@endif
				</div> <!-- end .col-sm-3 -->
			</div> <!-- end .row -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->

@endsection

@section('scripts')
@endsection
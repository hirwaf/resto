
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> @yield('title') </title>

  <!-- Bootstrap core CSS -->

  <link href="{{ asset(url('css/bootstrap.min.css')) }}" rel="stylesheet">

  <link href="{{ asset(url('fonts/css/font-awesome.min.css')) }}" rel="stylesheet">
  <link href="{{ asset(url('css/animate.min.css')) }}" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="{{ asset(url('css/custom.css')) }}" rel="stylesheet">
  <link href="{{ asset(url('css/icheck/flat/green.css')) }}" rel="stylesheet">
  @yield('styles')

  <script src="{{ asset(url('js/jquery.min.js')) }}"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>TCC</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <!-- <img src="{{ asset(url('images/img.jpg')) }}" alt="..." class="img-circle profile_img"> -->
              <div class="profile_img img-circle text-center" style="
                width: 56.34px;
                height: 56.34px;
                background: #16A085;
                color: #ECF0F1;
                font-size:xx-large;
                font-weight: 500;
                border: 2px #ECF0F1 solid;  " >
                {{ ucfirst($get->admin('name'){0} ) }}
              </div>
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>{{ ucfirst($get->admin('name')) }}</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          <!-- sidebar menu -->
          @yield('sidebar')
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  {{ ucfirst($get->admin('name')) }}
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="{{ url(route('profile.admin')) }}">  Profile</a>
                  </li>
                  <li><a href="{{ url(route('logout.admin')) }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          @yield('content')
        </div>

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">
              Toundra - Developed By <a href="http://www.elix.com">hirwaf</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="{{ asset(url('js/bootstrap.min.js')) }}"></script>

  <!-- bootstrap progress js -->
  <script src="{{ asset(url('js/progressbar/bootstrap-progressbar.min.js')) }}"></script>
  <script src="{{ asset(url('js/nicescroll/jquery.nicescroll.min.js')) }}"></script>
  <!-- icheck -->
  <script src="{{ asset(url('js/icheck/icheck.min.js')) }}"></script>

  <script src="{{ asset(url('js/custom.js')) }}"></script>

  <!-- pace -->
  <script src="{{ asset(url('js/pace/pace.min.js')) }}"></script>
  @yield('scrpits')
</body>

</html>
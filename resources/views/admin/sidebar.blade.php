  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">
      <h3>&nbsp;</h3>
      <ul class="nav side-menu">
        
        <li>
          <a href="{{ url(route('dashboard.admin')) }}">
            <i class="fa fa-home"></i> Dashboard
          </a>
        </li>
        
        <li>
          <a  href="{{ url(route('pages.admin')) }}" ><i class="fa fa-file-o"></i> Pages </a>
        </li>
        <li>
          <a><i class="fa fa-align-justify"></i> Menu <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="display: none">
            <li><a href="{{ url(route('cats.admin')) }}">Add Category</a></li>
            <li><a href="{{ url(route('menus.admin')) }}">All Items </a></li>
          </ul>
        </li>
        <li>
          <a><i class="fa fa-newspaper-o"></i> Blog <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="display: none">
            <li><a href="{{ url(route('blogs.admin')) }}">All</a></li>
            <li><a href="{{ url(route('create.blog.admin')) }}">Add</a></li>
          </ul>
        </li>
        <li>
          <a  href="{{ url(route('events.admin')) }}" ><i class="fa fa-calendar"></i> Events </a>
        </li>
        <li>
          <a  href="{{ url(route('videos.admin')) }}" ><i class="fa fa-youtube-play"></i> Videos </a>
        </li>
        
        <li>
          <a><i class="fa fa-users"></i> Employees <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="display: none">
            <li>
              <a href="{{ url(route('employees.admin')) }}">List</a>
            </li>
            <li>
              <a href="{{ url(route('create.employee.admin')) }}">New</a>
            </li>
          </ul>
        </li>
        <div class="menu_section">
          <h4>&nbsp;</h4>
          <ul class="nav side-menu">
            <li>
              <a href="{{ url(route('cogs.admin')) }}"><i class="fa fa-align-left"></i> Site Informations </a>
            </li>
            <li>
              <a href="{{ url(route('style.cogs.admin')) }}"><i class="fa fa-th"></i> Site Style </a>
            </li>
            <li>
              <a href="{{ url(route('toundra.inbox.admin')) }}"><i class="fa fa-inbox"></i> Messages <span class="badge pull-right"></span> </a>
            </li>
        </ul>
        </div>
      </ul>
    </div>
  </div>
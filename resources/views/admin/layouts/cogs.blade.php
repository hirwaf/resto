@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
			  <h2>Site Information <small>Toundra Coffee Cup</small></h2>
			  <div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				{!! Form::open(['route' => 'store.cog.admin', 'files' => true ,'class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '', 'accept-charset' => '']) !!}
					<div class="form-group">
						{{ Form::label('name','Site Name',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('name',$cog->siteinfo('name'),[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('icon','Site Icon',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::file('icon',null,[ 'class' => 'form-control','autocomplete' => 'off' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('email','Site Email',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::email('email',$cog->siteinfo('email'),[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="booking">Booking Telephone</label>
                      	<div class="col-md-8 col-sm-8 col-xs-12">
                        	<input type="text" name="booking" id="booking" class="form-control" data-inputmask="'mask' : '250 799 999999'" value="{{ $cog->siteinfo('booking') }}" >
                      	</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="information">Customer Care</label>
                      	<div class="col-md-8 col-sm-8 col-xs-12">
                        	<input type="text" name="information" id="information" class="form-control" data-inputmask="'mask' : '250 799 999999'" value="{{ $cog->siteinfo('care') }}" >
                      	</div>
					</div>
					<h5 class="col-sm-offset-3">Social Networks</h5>
					<div class="form-group">
						{{ Form::label('facebook','Facebook',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('facebook',$cog->siteinfo('facebook'),[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('pinterest','Pinterest',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('pinterest',$cog->siteinfo('pinterest'),[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('instagram','Instagram',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('instagram',$cog->siteinfo('instagram'),[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('','Open Hours',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::label('days','Monday - Friday',['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) }}
							{{ Form::text('days',$cog->siteinfo('days'), [ 'class' => 'form-control' ]) }}
							{{ Form::label('saturday','Saturdays',['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) }}
							{{ Form::text('saturday',$cog->siteinfo('suturday'), [ 'class' => 'form-control' ]) }}
							{{ Form::label('sunday','Sundays',['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) }}
							{{ Form::text('sunday',$cog->siteinfo('sunday'), [ 'class' => 'form-control' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('description','Site Description',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::textarea('description',$cog->siteinfo('description'),[ 'class' => 'form-control','rows'=>'2' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::submit('Save changes',[ 'class' => 'btn btn-primary col-md-offset-3'  ]) }}
					</div>
				{!! Form::close() !!}
			</div>		
		</div>
	</div>
</div>
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: false,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif

@endsection
@section('styles')
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
@endsection
@section('scrpits')
<!-- input mask -->
<script src="{{ asset(url('js/input_mask/jquery.inputmask.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
<!-- input_mask -->
<script>
$(document).ready(function() {
  	$(":input").inputmask();
});
</script>
@endsection
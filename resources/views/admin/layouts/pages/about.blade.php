<div class="row">
	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit {{ ucfirst($slug) }} Page <small>of Toudra Coffee Cup</small></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				{!! Form::open([ 'route' => 'page.position.admin', 'files' => true, 'class' => '' ]) !!}
					{{ Form::hidden('where', 'u') }}
					<div class="row">
						<div class="col-md-6">
							{{ Form::text( 'small', $about['small'], [ 'class' => 'form-control' ] ) }}
							<div class="clearfix">&nbsp;</div>
							{{ Form::text( 'normal', $about['normal'], [ 'class' => 'form-control' ] ) }}
							<div class="clearfix">&nbsp;</div>
							{{ Form::textarea('text', $about['text'],[ 'class' => 'form-control', 'rows' => '12' ]) }}
							<div class="clearfix">&nbsp;</div>
						</div>
						<div class="col-md-6">
							<input name="image" type="file" id="image" class="form-control" >
							<div class="clearfix">&nbsp;</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<img src="{{ asset(url('images/web/'.$about['image'])) }}" class="img img-resiponsive" >
								</div>
							</div>
						</div>
						<div class="clearfix">&nbsp;</div>
						<div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4 ">
							{{  Form::submit('Save changes',[ 'class' => 'btn btn-primary btn-block' ]) }}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
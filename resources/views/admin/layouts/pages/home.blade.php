<div class="row">
	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Welcome Page  <small>of Toudra Coffee Cup</small> </h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				@if(!empty($slides))
					<h5>Home Slides <i class="fa fa-angle-double-down"></i></h5>
					<div class="row">
						@foreach( $slides as $key => $slide )
							<div class="col-md-2 col-sm-2" style="" >
								<img src="{{ asset(url('images/web/slides/'.$slide['image'])) }}" data-caption="{{ $slide['caption'] }}" class="img img-responsive slide" style="cursor:pointer;">
								<center><label class="label label-success">{{ ++$key }}</label></center>
							</div>
						@endforeach	
					</div>
					<div class="clearfix">&nbsp;</div>
				@endif
				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12">
						<h4>Upload New or Replace Image </h4>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								{!! Form::open(['route' => 'page.position.admin', 'files' => true ,'class' => 'form-horizontal form-label-left']) !!}
								<div class="form-group">
									{{ Form::label('new','Replace or New',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										<?php $in = count($slides)  < 5 ? count($slides) <= 0 ? ['New'] : array_merge(range(1,count($slides)),['New']) : range(1,5) ?>
										{{ Form::select('new', $in, null, [ 'class' => 'form-control' ]) }}
									</div>
								</div>
								{{ Form::hidden('where', 'w') }}
								<div class="form-group">
									{{ Form::label('caption','Caption',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										{{ Form::text('caption',null,[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form::label('image','Image',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										{{ Form::file('image',null,[ 'class' => 'form-control','autocomplete' => 'off' ]) }}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 col-md-offset-5">
										{{ Form::submit( empty($slides) ? 'Add new' : 'Save changes', ['class' => 'btn btn-primary btn-block'] ) }}
									</div>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade view-full">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Full Scale</h4>
      </div>
      <div class="modal-body">

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(function(){
		$('*.slide').on('click',function(e){
			var image = $(this).attr('src');
			var caption = $(this).attr('data-caption');
			$('.view-full .modal-body').html("<img src='"+image+"' class='img img-responsive'><br><p class='text-quo text-sm'>"+caption+"</p>");
			$('.view-full').modal();
		});
	});
</script>
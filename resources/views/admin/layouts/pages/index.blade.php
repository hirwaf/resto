@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
      <div class="x_panel">
        <div class="x_title">
          <h2>Pages <small>Of Toundra Coffee Cup</small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th>Position</th>
                <th width="100"></th>
              </tr>
            </thead>
            <tbody id='in-table'>
            @if($pages->first() != null)
            	@foreach($pages as $page)
            		<tr>
            			<td>
            				<span style="
            					cursor: pointer;
            					
            				">
	            				<b class="{{ in_array($page->slug,$not)  ? 'label label-success '  : 'page-name label label-primary'  }}" data-url="{{ url(route('edit.page.admin',$page->id)) }}">
	            					{{ $page->name }}&nbsp;
	            					<i class="{{ in_array($page->slug,$not) ? '' : 'fa fa-external-link' }}" ></i>
	            				</b>
            				</span>
            			</td>
            			<td>
            				<button type="button" value="{{ $page->id }}" class="btn btn-flat btn-block btn-primary btn-position" >{{ $page->position }}</button>
            				<?php postion($page->id,$page->name,$page->position); ?>
            			</td>
            			<td>
            				<button type="button" value="{{ $page->id }}" class="btn btn-flat btn-md pull-right btn-enabled  {{ $page->enabled ? 'btn-danger' : 'btn-primary' }}">{{ $page->enabled ? 'Disable' : 'Enable' }}</button>
            				<?php enabled($page->id,$page->name,$page->enabled); ?>
            			</td>
            		</tr>
            	@endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection
@section('styles')
<style type="text/css">
	#in-table .label{
		padding: 1em;
	}
	.link{
		text-decoration: unset;
	}
</style>
@endsection
@section('scrpits')
<script src="{{ asset(url('js/datatables/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.bootstrap.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.buttons.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/jszip.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/pdfmake.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/vfs_fonts.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.print.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.fixedHeader.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.keyTable.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/responsive.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.scroller.min.js')) }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').dataTable();
    $('* .btn-position').on('click',function(e){
    	var v = e.target.value;
    	$('.position-modal-'+v).modal();
    });
    $('* .btn-enabled').on('click',function(e){
    	var v = e.target.value;
    	$('.enabled-modal-'+v).modal();
    });
    $('* .page-name ').on('click', function(e){
    	var url = $(this).attr('data-url')+"<?= '?_dt='.session()->get('page_edit') ?>";
    	window.open(url,'_self');
    });

  });
</script>
@endsection
<?php
	function postion($id,$title,$position = 1)
	{
?>
		<div class="modal fade position-modal-{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		        <h4 class="modal-title" id="myModalLabel2">Change {{ ucfirst($title) }} Position </h4>
		      </div>
		      {!! Form::open(['route' => 'page.position.admin']) !!}
		      <div class="modal-body">
		      	{{ Form::label('position', 'Select Position', ['class' => 'label-control']) }}
		      	<select class='form-control' name='position'>
		      		@foreach(range(1,7) as $pos )
		      			<option value='{{ $pos }}' {{ $position == $pos ? "selected" : "" }} >{{ $pos }}</option>
		      		@endforeach
		      	</select>
		      	{{ Form::hidden('page', $id) }}
		      	{{ Form::hidden('where', 'p') }}
		      	
		      </div>
		      <div class="modal-footer">
		      	{{ Form::button('Close',[ 'class' => 'btn btn-default', 'data-dismiss' => 'modal' ]) }}
		      	{{ Form::submit('Save changes',[ 'class' => 'btn btn-primary' ]) }}
		      </div>
		    </div>
		    {{ Form::close() }}
		  </div>
		</div>
<?php
	}
	function enabled($id,$title,$status){
?>
		<div class="modal fade enabled-modal-{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		        <h4 class="modal-title" id="myModalLabel2">Are You Sure</h4>
		      </div>
		      {!! Form::open(['route' => 'page.position.admin']) !!}
		      <div class="modal-body">
		      	{{ Form::hidden('page', $id) }}
		      	{{ Form::hidden('where', 's') }}
		      	<h3> {{ $status ? "Disable" : "Enable" }} <i>{{ strtoupper($title) }}</i> </h3>
		      </div>
		      <div class="modal-footer">
		      	{{ Form::button('Close',[ 'class' => 'btn btn-default', 'data-dismiss' => 'modal' ]) }}
		      	{{ Form::submit('Save changes',[ 'class' => 'btn btn-primary' ]) }}
		      </div>
		    </div>
		    {{ Form::close() }}
		  </div>
		</div>
<?php		
	}
?>
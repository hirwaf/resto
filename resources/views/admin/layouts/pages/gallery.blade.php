<div class="row">
	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Gallery Page  <small>of Toudra Coffee Cup</small> </h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12">
						<h4>Upload New Image </h4>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								{!! Form::open(['route' => 'page.position.admin', 'files' => true ,'class' => 'form-horizontal form-label-left']) !!}
								{{ Form::hidden('where', 'g') }}
								<div class="form-group">
									{{ Form::label('category','Category *',[ 'class'	=>	'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										<select name="category" class="select2_single form-control" required >
											@if( count($categories) > 0 )
												@foreach( $categories as $category )
													<option value="{{ $category['id'] }}">{{ ucfirst($category['name']) }}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>
								
								<div class="form-group">
									{{ Form::label('title','Title',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										{{ Form::text('title',null,[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
									</div>
								</div>

								<div class="form-group">
									{{ Form::label('caption','Caption',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										{{ Form::text('caption',null,[ 'class' => 'form-control', 'autocomplete' => 'off' ]) }}
									</div>
								</div>
								
								<div class="form-group">
									{{ Form::label('image','Image *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
									<div class="col-md-8 col-sm-8 col-xs-12">
										{{ Form::file('image',[ 'class' => 'form-control', 'required' => '' ]) }}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 col-md-offset-5">
										{{ Form::submit( "Add new", ['class' => 'btn btn-primary btn-block'] ) }}
									</div>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
				@if(!empty($images))
					<h5>Images in Gallery Page <i class="fa fa-angle-double-down"></i></h5>
					<div class="row gallery-h">
						@foreach( $images as $key => $image )
							<div class="col-md-2 col-sm-2" style="" >
								<img src="{{ asset(url('images/web/gallery/'.$image['url'])) }}" title="{{ $image['title'] }}" data-caption="{{ $image['caption'] }}" data-category="{{ ucfirst($image->category()->first()->name) }}" class="img img-responsive slide" style="cursor:pointer;">
								<center>
								{!! Form::open( [ 'route' => 'page.position.admin', 'class' => 'form-'.$image['id'] ] ) !!}
									{{ Form::hidden('id',$image['id'] ) }}
									{{ Form::hidden('where','di') }}
									<button type="submit" class="label label-danger del-image" data-id="{{ $image['id'] }}" style="cursor: pointer;">Delete</button>
								{!! Form::close() !!}
								</center>
							</div>
						@endforeach
					</div>
					<div class="clearfix">&nbsp;</div>
				@endif				
			</div>
		</div>
	</div>
</div>

<div class="modal fade view-full">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Full Scale</h4>
      </div>
      <div class="modal-body">

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(function(){
		$('*.slide').on('click',function(e){
			var image = $(this).attr('src');
			var caption = $(this).attr('data-caption');
			var category = $(this).attr('data-category');
			var title = $(this).attr('title');
			$('.view-full .modal-body').html("<img src='"+image+"' class='img img-responsive'><br><p class='text-quo text-sm'> Title: "+title+"  </p><p class='text-quo text-sm'> Category: "+category+"  </p><p class='text-quo text-sm'> Caption:"+caption+"</p><br>");
			$('.view-full').modal();
		});
		$('* .del-image').on('click', function(e){
			var v = $(this).attr('data-id');
			if ( confirm('Are you sure \nYou want to delete this image !!') === true ) {
				$('.form-')				
			}
			else 
				return false;
		});
	});
</script>
<script>
$(document).ready(function() {
  $(".select2_single").select2({
    placeholder: "Category name",
    allowClear: true
  });
});
</script>
<style type="text/css">
	.gallery-h{
		height: 190px;
		background-color: #ECF0F1;
		border-radius: 0.15em;
		padding-top: 10px;
		overflow-y: auto;
	}
	.del-image{
		border: none;
	}
</style>
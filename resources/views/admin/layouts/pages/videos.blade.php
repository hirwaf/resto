<?php $in = session()->has('errors') ? true : false ?>
@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a href="#addVideo" role="button" data-toggle="collapse" aria-expanded="{{ $edit || $in ? 'true' : 'false' }}" aria-controls="addVideo" >
						{{ $title }} Video
					</a>
					<small> for Toundra Coffee Cup </small>
				</h2>
				<div class="clearfix">&nbsp;</div>			
			</div>
			<div class="x_content collapse {{ $edit || $in ? 'in' : '' }}" id="addVideo" >
				{{ Form::open([ 'route' => $route, 'files' => 'true', 'class' => 'form-horizontal form-label-left' ]) }}
					@if( $edit )
						{{ Form::hidden('id',$video->id) }}
					@endif
					<div class="form-group">
						{{ Form::label('title','Video Title *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('title', $edit ? $video->title : old('title') ,['class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('url','Video URL *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							@if( $edit )
								{{ Form::hidden('url', $video->net_url ) }}
								<input type="text" class="form-control" value="{{ $video->net_url }}" disabled="true" >
								<small class="text text-sm text-info"> If you want to update this field delete the video and start over ! </small>
							@else
								{{ Form::text('url', $edit ? $video->net_url : old('url') ,['class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ]) }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-8 col-md-offset-3 col-sm-8 col-xs-12">
							{{ Form::submit($button, [ 'class' => 'btn btn-sm btn-primary' ]) }}
						</div>
					</div>

				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a href="#allVideo"  role="button" data-toggle="collapse" aria-expanded="{{ $edit || $in ? 'false' : 'true' }}" aria-controls="allVideo">
						All Videos
					</a>
					<small> for Toundra Coffee Cup </small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content collapse {{ $edit || $in ? '' : 'in' }}" id="allVideo">
				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<th>Name</th>
						<th width="50"></th>
					</thead>
					<tbody>
						@if( count($videos) >= 1  )
							@foreach( $videos as $video )
								<tr>
									<td>
										<div class="row">
											<div class="col-sm-3 img-responsive">
												<img src="{{ $video->name }}" class="img img-responsive">
											</div>
											<div class="col-sm-8">
												<h4 style="cursor: pointer;" class="video" data-url="{{ $video->net_url }}" title="{{ $video->title }}" >{{ $video->title }}</h4>
											</div>
										</div>
									</td>
									<td>
										<a href="{{ url(route('edit.video.admin',$video->id)) }}" class="btn btn-link btn-md"> Edit </a>
										<button class="btn btn-danger btn-xs btn-del" data-video="{{ $video->id }}" >Delete</button>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="viewVideo">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">View Video</h4>
      </div>
      <div class="modal-body"></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scrpits')
<!-- Validator -->
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- pace -->
<script src="{{ asset(url('js/pace/pace.min.js')) }}"></script>
<!-- Datatables-->
<script src="{{ asset(url('js/datatables/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.bootstrap.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.buttons.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/jszip.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/pdfmake.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/vfs_fonts.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.print.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.fixedHeader.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.keyTable.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/responsive.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.scroller.min.js')) }}"></script>
<!-- Validate Form -->
<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	    $('#datatable-responsive').DataTable({
			keys: true,
			sort: true
		});
	});
</script>
@endif
<script type="text/javascript">
	$(function(){
		$("*.btn-del").on('click',function(e){
			var v = $(this).attr("data-video");
			if ( confirm("Are sure \nYou want to delete this video !! ") == true ) {
		  		$.post(
			  		"{{ url(route('destroy.video.admin')) }}",
			  		{
			  			id : v,
			  			_token : "{{ csrf_token() }}"
			  		},function(data){
			  			window.location = data;
			  		}
			  	);
		  	}
		  	else{
		  		return false;
		  	};
		});
		$("*.video").on('click', function(e){
			var u = $(this).attr('data-url');
			var title = $(this).attr('title');
			var html = "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='"+u+"'></iframe></div>";
			var modal = $("#viewVideo");
			modal.modal();
			modal.find('.modal-title').text(title);
			modal.find('.modal-body').html(html);
		});
	});
</script>
@endsection
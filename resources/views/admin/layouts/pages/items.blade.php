@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-9 col-md-offset-1 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"  aria-expanded="{{ $edit ? 'true' : 'false' }}" aria-controls="collapseThree">
						{{ $title }} Item
					</a> 
					<small> for Toundra Coffee Cup</small>
				</h2>
			  	<div class="clearfix"></div>			
			</div>
			<div id="collapseThree" class="x_content panel-collapse collapse {{ $edit ? 'in' : '' }}" role="tabpanel" aria-labelledby="headingThree">
				{!! Form::open([ 'route' => $route, 'files' => true, 'class' =>	'form-horizontal form-label-left' ]) !!}
					@if( $edit )
						{{ Form::hidden('id',$item->id) }}
					@endif
					<div class="form-group">
						{{ Form::label('name','Item Name *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('name', $edit ? $item->name : old('name') ,['class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('category','Category *',[ 'class' =>	'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-8 col-sm-8 col-xs-12" >
							<select name="category" class="select2_single form-control" style="width: 100% !important;" required>
								@if( count($items) > 0 )
									@foreach( $items as $category )
										<option value="{{ $category['id'] }}" {{ $edit ? $item->category_id == $category->id ? "selected" : "" : "" }} >{{ ucfirst($category['name']) }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('price[r]', 'Item Price *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<table class="table table-bordered">
								<thead>
									<th class="text-center">R <span class="required">*</span></th>
									<th class="text-center">L</th>
								</thead>
								<tbody>
									<tr>
										<td class="active">
											{{ Form::text('price[r]',$edit ? json_decode($item->price,true)['r'] : old('price.r'), ['class' => 'form-control', 'required' => '', 'autocomplete' => 'off' ] ) }}
										</td>
										<td class="active">
											{{ Form::text('price[l]',$edit ? json_decode($item->price,true)['l'] : old('price.l'), ['class' => 'form-control', 'autocomplete' => 'off' ] ) }}
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('image','Image *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{{ Form::file('image',[ 'class' => 'form-control', $edit ? '' : 'required' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('special','Our Specialty *',[ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-6 col-sm-6 col-xs-12 ">
							<div class="row">
								<div class="checkbox">
		                          <label>
		                            <input type="checkbox" class="flat" {{ $edit ? $item->special ? "checked": "" : "" }}  name="special" >
		                          </label>
		                        </div>
							</div>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('description', 'Description *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::textarea('description',$edit ? $item->description : old('description'), [ 'class' => 'form-control', 'required' => '', 'rows' => '5' ] ) }}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">&nbsp;</div>
						<div class="col-md-6 col-sm-6 col-xs-12 ">
							<button class="btn btn-primary">{{ $button }} Item</button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-9 col-md-offset-1 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="{{ $edit ? 'false' : 'true' }}" aria-controls="collapseTwo">
						List Of Items
					</a>
					<small>for Toundra Coffee Cup </small>
				</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content panel-collapse collapse {{ $edit ? '' : 'in' }}" id='collapseTwo' role="tabpanel" aria-labelledby="headingTwo">
				<table class="table table-bordered" >
					<thead>
						<th>Name</th>
						<th>Price</th>
						<th width="50"></th>
					</thead>
					<tbody>
						@if( $items )
							@foreach( $items as $item )
								<tr>
									@if( $item->menu()->count() >= 1 )
									<td colspan="4" class="text-center active" > <b>{{ strtoupper($item->name) }}</b> </td>
								</tr>
										@foreach( $item->menu()->get() as $menu )
											<tr>
												<td> <label class="label label-primary name"> {{ ucfirst($menu->name) }} </label> </td>
												<td> <?php $price = json_decode( $menu->price, true); ?>
													@if( $price['r'] != "" )
														<label class="label label-primary">
															R : {{ $price['r'] }}
														</label>
													@endif
													&nbsp;
													@if( $price['l'] != "" )
														<label class="label label-primary">
															L : {{ $price['l'] }}
														</label>
													@endif
												</td>
												<td>
													<a href="{{ route('edit.menu.admin',$menu->id) }}" class="btn btn-xs btn-success" > Edit </a>
												</td>
											</tr>
										@endforeach
									@endif
							@endforeach
						@endif
					</tbody>
				</table>			 
			</div>
		</div>
	</div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
<!-- select2 -->
<link href="{{ asset(url('css/select/select2.min.css')) }}" rel="stylesheet">
<!-- switchery -->
<link rel="stylesheet" href="{{ asset(url('css/switchery/switchery.min.css')) }}" />
<style type="text/css">
	.name{
		text-rendering: all;
		text-transform: capitalize;
		font-size: x-small;
		padding: .5em;
	}
</style>
@endsection
@section('scrpits')
<!-- switchery -->
<script src="{{ asset(url('js/switchery/switchery.min.js')) }}"></script>
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- jQuery.filer -->
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
<!-- select2 -->
<script src="{{ asset(url('js/select/select2.full.js')) }}"></script>

<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>
<script>
	$(document).ready(function() {
	  $(".select2_single").select2({
	    placeholder: "Category name",
	    allowClear: true,
	  });
	});
</script>
<script type="text/javascript">
	$(function(){
		$(' *.btn-more ').on('click',function(e){
			var v = $(this).attr('value');
			alert(v);
		});
	});
</script>
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: false,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@endsection

@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
		<div class="x_panel">
			<div class="x_title">
				<h2>Add New Category <small>of Toudra Coffee Cup</small> </h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				{!! Form::open( [ 'route'	=>	'post.cat.admin', 'class'	=>	'form-horizontal form-label-left' ] ) !!}
					<div class="form-group" >
						<div class="row">
							<div class="radio col-md-2">
	                          <label>
	                            <input type="radio" class="flat" checked name="is" value="1" > Drinks
	                          </label>
	                        </div>
	                        <div class="radio col-md-2">
	                          <label>
	                            <input type="radio" class="flat"  name="is" value="2" > Foods
	                          </label>
	                        </div>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('categoryName', 'Category Name', [ 'class' => 'label-control' ]) }}
						{{ Form::text('categoryName',old('categoryName'),[ 'class'	=> 'form-control', 'autocomplete' => 'off' ]) }}
					</div>
					<div class="form-group">
						<div class="col-md-4 col-sm-4 col-md-offset-4">
							{!! Form::submit('Save category',[ 'class' => 'btn btn-primary btn-block' ]) !!}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="x_panel">
			<div class="x_title">
				<h2>Registered Categories <small>of Toudra Coffee Cup</small> </h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
	                    <tr>
	                      <th width="30">#</th>	
	                      <th width="500">Category name</th>
	                      <th>Has</th>
	                      <th>In</th>
	                      <th width="30">Action</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	@if( count($categories) > 0  )
	                		<?php $a = 1; ?>
	                		@foreach( $categories as $category )
	                			<?php  $p = $category->menu()->count() <= 1  ? "" : "s"; ?>
	                			<tr>
	                				<td> <label class="label label-primary">{{ $a++ }} <i class="fa fa-caret-right"></i>&nbsp; </label></td>
	                				<td> <b class="label label-primary">{{ strtoupper($category->name) }}</b> </td>
	                				<td> <label class="label label-primary">{{ $category->menu()->count()." Item".$p }}</label> </td>
	                				<td> <label class="label label-primary">{{ $category->is == 1? "Drinks" : "Foods" }}</label></td>
	                				<td>
	                					<button class="btn btn-link btn-xs btn-view" value="{{ $category->id }}" >View</button>
	                					<?php viewme($category); ?>
	                				</td>
	                			</tr>
	                		@endforeach
	                	@endif
	                </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@if(session()->has('errors'))
<?php $errors_ = session()->pull('errors'); ?>	
  <script type="text/javascript">
    var permanotice, tooltip, _alert;
    $(function() {
      new PNotify({
        title: "Errors",
        type: "error",
        text: "@foreach($errors_ as $error ) {{ $error }} \n @endforeach",
        nonblock: {
          nonblock: true
        }
      });

    });
  </script>	
@endif
@endsection
@section('styles')
<!-- select2 -->
<link href="{{ asset(url('css/select/select2.min.css')) }}" rel="stylesheet">
<!-- switchery -->
<link rel="stylesheet" href="{{ asset(url('css/switchery/switchery.min.css')) }}" />
<style type="text/css">
	.label-success{
		padding: 0.9em;
	}
</style>
@endsection
@section('scrpits')
<!-- switchery -->
<script src="{{ asset(url('js/switchery/switchery.min.js')) }}"></script>
<!-- select2 -->
<script src="{{ asset(url('js/select/select2.full.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- Datatables-->
<script src="{{ asset(url('js/datatables/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.bootstrap.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.buttons.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/jszip.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/pdfmake.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/vfs_fonts.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.print.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.fixedHeader.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.keyTable.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/responsive.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.scroller.min.js')) }}"></script>
<script type="text/javascript">
	$(function(){
		$('#datatable-responsive').DataTable({
			keys: true,
			sort: true
		});

		$('* .btn-view').on('click',function(e){
			var v = $(this).attr('value');
			$('.modal-'+v).modal();
		});
	});
</script>
@endsection
<?php
	function viewme($category)
	{
?>
<div class="modal fade modal-{{$category->id}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit <i> {{ strtoupper($category->name) }} </i> </h4>
      </div>
      <div class="modal-body">
      	{!! Form::open([ 'route' => 'post.cat.admin', 'class' => 'form-horizontal form-label-left'  ]) !!}
      		{{ Form::hidden('id',$category->id) }}
      		<div class='row'>
      			<div class='col-sm-12'>
	      			<div class="form-group" >
						<div class="">
							<div class="radio col-md-7 col-sm-7">
		                      <label>
		                        <input type="radio" class="flat" {{ $category->is == '1' ? "checked" : "" }} name="is" value="1" > Drinks
		                      </label>
		                    </div>
		                    <div class="radio col-md-6 col-sm-6">
		                      <label>
		                        <input type="radio" class="flat" {{ $category->is == '2' ? "checked" : "" }} name="is" value="2" > Foods
		                      </label>
		                    </div>
						</div>
					</div>	
      			</div>
      		</div>
      		<div class='clearfix'>&nbsp;</div>
			<div class="form-group row">
				{{ Form::label('categoryName', 'Category Name', [ 'class' => 'label-control' ]) }}
				{{ Form::text('categoryName',$category->name,[ 'class'	=> 'form-control', 'autocomplete' => 'off' ]) }}
			</div>
			<div class='clearfix'>&nbsp;</div>
			<div class='form-group'>
				Has Item(s) : {{ $category->menu()->count() }} 
			</div>
			<div class='clearfix'>&nbsp;</div>
			<div class='form-group'>
				<div class="checkbox">
                  <label>
                    <input type="checkbox" class="flat" name='delete' > Delete this category
                  </label>
                </div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<?php		
	}
?>
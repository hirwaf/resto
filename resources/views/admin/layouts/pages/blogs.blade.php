@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12" id="accordion" role="tablist" aria-multiselectable="true" >
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"  aria-expanded="true" aria-controls="collapseThree">
						All Articles
					</a>
					<small> For Toundra Coffee Cup  </small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content panel-collapse collapse in" role="tabpanel" id="collapseThree" >
				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Articles</th>
				</thead>
				<tbody>
					@if( count($articles) >= 1 )
						@foreach( $articles as $article )
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-2">
											<img src="{{ asset(url('/images/web/blogs/'.$article->image)) }}" class="img-responsive img-thumbnail">
										</div>
										<div class="col-sm-9">
											<h4> <a href="{{ url('/blog/'.$article->slug) }}" target="__blank" >{{ ucfirst($article->title) }}</a> </h4>
											<p class="p">
												{{ ucfirst($article->excerpt) }}
											</p>
											<p>
												<label class="label label-primary"> Author : {{ ucfirst( $article->author ) }} </label> &nbsp;
												<label class="label label-primary"> {{ $article->created_at->diffForHumans() }} </label>
											</p>
										</div>
										<div class="col-sm-1">
											<a href="{{ url(route('edit.blog.admin',$article->id)) }}" class="btn btn-xs btn-block btn-success" >
												edit
											</a>
											<button class="btn btn-xs btn-danger btn-del" data-article="{{ $article->id }}" >Delete</button>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
				</table>
			</div>
		</div>
		<div class="x_panel">
			<div class="x_title" role='tab' >
				<h2>
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"  aria-expanded="false" aria-controls="collapseTwo">
						Draft Articles
					</a>
					<small> For Toundra Coffee Cup  </small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content panel-collapse collapse" role="tabpanel" id="collapseTwo" >
				<table id="datatable-responsive-2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Articles</th>
				</thead>
				<tbody>
					@if( count($drafts) >= 1 )
						@foreach( $drafts as $article )
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-2">
											<img src="{{ asset(url('/images/web/blogs/'.$article->image)) }}" class="img-responsive img-thumbnail">
										</div>
										<div class="col-sm-9">
											<h4> <a href="{{ url('/blog/'.$article->slug) }}" target="__blank" >{{ ucfirst($article->title) }}</a> </h4>
											<p class="p">
												{{ ucfirst($article->excerpt) }}
											</p>
											<p>
												<label class="label label-primary"> Author : {{ ucfirst( $article->author ) }} </label> &nbsp;
												<label class="label label-primary"> {{ $article->created_at->diffForHumans() }} </label>
											</p>
										</div>
										<div class="col-sm-1">
											<a href="{{ url(route('edit.blog.admin',$article->id)) }}" class="btn btn-xs btn-block btn-success" >
												edit
											</a>
											<button class="btn btn-xs btn-danger btn-del" data-article="{{ $article->id }}" >Delete</button>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('styles')
@endsection

@section('scrpits')
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- pace -->
<script src="{{ asset(url('js/pace/pace.min.js')) }}"></script>
<!-- Datatables-->
<script src="{{ asset(url('js/datatables/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.bootstrap.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.buttons.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/jszip.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/pdfmake.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/vfs_fonts.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.print.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.fixedHeader.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.keyTable.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/responsive.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.scroller.min.js')) }}"></script>

<script type="text/javascript">
	$(function(){
		$("*.btn-del").on('click',function(e){
			var v = $(this).attr("data-article");
			if ( confirm("Are sure \nYou want to delete this article !! ") == true ) {
		  		$.post(
			  		"{{ url(route('destroy.blog.admin')) }}",
			  		{
			  			id : v,
			  			_token : "{{ csrf_token() }}"
			  		},function(data){
			  			window.location = data;
			  		}
			  	);
		  	}
		  	else{
		  		return false;
		  	};
		});
		$('#datatable-responsive').DataTable({
			keys: true,
			sort: false
		});
	});
</script>

@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	    $('#datatable-responsive,#datatable-responsive-2').DataTable({
			keys: true,
			sort: true
		});
	});
</script>
@endif
@endsection
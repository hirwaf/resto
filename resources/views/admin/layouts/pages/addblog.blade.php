@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-12 col-md-offset-0 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					{{ $title }} Blog
					<small>For Toundra Coffee Cup</small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content">
				{{ Form::open([ 'route' => $route, 'files' => true, 'class' => 'form form-label-left'   ]) }}
				@if( $edit )
					{{ Form::hidden('id',$blog->id) }}
				@endif
					<div class="row">
						<div class="col-sm-9">
							<div class="form-group">
								{{ Form::label('author','Author *', [ 'class' => 'control-label col-md-12 col-sm-12 col-xs-12' ] ) }}
								<div class="col-md-12 col-sm-12 col-xs-12">
									{{ Form::text('author', $edit ? $blog->author : old('author') == "" ? $get->admin('name') : old('author')  , [ 'class' => 'form-control', 'required' => '' ] ) }}
									<small class="text text-info">If author is not you please change this field <i class="fa fa-long-arrow-up"></i> </small>
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('title','Title *', [ 'class' => 'control-label col-md-12 col-sm-12 col-xs-12' ] ) }}
								<div class="col-md-12 col-sm-12 col-xs-12">
									{{ Form::text('title', $edit ? $blog->title : old('title'), [ 'class' => 'form-control', 'required' => '' ] ) }}
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('slug','Slug *', [ 'class' => 'control-label col-md-12 col-sm-12 col-xs-12' ] ) }}
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="input-group">	
										<span class="input-group-addon"> {{ url('/')."/blog/" }}  </span>
										{{ Form::text('slug', $edit ? $blog->slug : old('slug'), [ 'class' => 'form-control', 'required' => '', 'readonly' => '', 'id' => 'slug' ] ) }}
									</div>
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('excerpt', "Excerpt *", [ 'class' => 'control-label col-md-12 col-sm-12 col-xs-12' ]) }}
								<div class="col-md-12 col-sm-12 col-xs-12">
									{{ Form::textarea('excerpt', $edit ? $blog->excerpt : old('excerpt'), [ 'class' => 'form-control', 'required' => '', 'rows' => '3' ] ) }}
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('content','Content *', [ 'class' => 'control-label col-md-12 col-sm-12 col-xs-12' ] ) }}
								<div class="col-md-12 col-sm-12 col-xs-12">
									{{ Form::textarea('content', $edit ? $blog->content : old('content'), [ 'class' => 'form-control', 'required' => '' ] ) }}
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
								    <div class="panel-heading" role="tab" id="headingOne">
								      <h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								          Featured Image
								        </a>
								      </h4>
								    </div>
								    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								      <div class="panel-body">
								      	<div class="form-group">
								      		{{ Form::file('image',['class' => 'form-control', 'id' => 'image'  , 'autocomplete' => 'off', "".$edit ? 'data' : 'required'."" => '' ]) }}
								      	</div>
								      	@if( $edit )
									      	<div class="img img-responsive">
									      		<img src="{{ asset(url('images/web/blogs/'.$blog->image)) }}" class="img-responsive img-thumbnail" >
									      	</div>
								      	@endif
								      </div>
								    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTags" >
										<h6 class="panel-title">
											<a href="#collapseTags" role='button' data-toggle='collapse' data-parent='#accordion' aria-expanded='false' aria-controls='collapseTags' >
												Tags
											</a>
										</h6>
									</div>
									<div id="collapseTags" class="panel-collapse collapse in" role='tabpanel' aria-labelledby="headingTags" >
										<div class="panel-body">
											<div class="form-group">
												{{ Form::text('tags', $edit ? $blog->tags : old('tags'), [ 'class' => 'form-control', 'placeholder' => 'Add Tags', 'id' => 'tags' ]  ) }}
												<small class="text-muted">Separate with "," </small>
											</div>
										</div>
									</div>
								</div>	
								<div class="panel panel-default">
								    <div class="panel-heading" role="tab" id="headingOne">
								      <h6 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
								          Publish
								        </a>
								      </h6>
								    </div>
								    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
								      <div class="panel-body">
								      	<div class="form-group">
								      		<div class="checkbox">
											  <label>
											    <input type="checkbox" name="published" <?= $edit ? $blog->published == false ? "checked" : "" :"" ?> >
											    Save As Draft
											  </label>
											</div>
								      	</div>
								      	<div class="form-group">
								      		<div class="checkbox">
											  <label>
											    <input type="checkbox" name="from_net" {{ $edit ? $blog->from_net ? "checked" : "" :"" }} >
											    From Internet
											  </label>
											</div>
								      	</div>
								      	<div class="form-group">
								      		{{ Form::text('source', $edit ? $blog->source : old('source'), [ 'class' => 'form-control', 'placeholder' => 'URL' ] ) }}
								      	</div>
								      	<div class="clearfix">&nbsp;</div>
								      	<div class="form-group">
								      		{{ Form::submit($button,['class' => 'btn btn-md btn-block btn-success']) }}
								      	</div>
								      </div>
								    </div>
								</div>						
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
@section('styles')
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
<style type="text/css">
	.panel{
		border-radius: none !important;
		border: none !important;
		box-shadow: 0px 1px 1px #BDC3C7;
	}
	.panel-heading{
		border-top-left-radius: 0px;
		border-top-right-radius: 0px;
	}
</style>
@endsection
@section('scrpits')
<!-- pace -->
<script src="{{ asset(url('js/pace/pace.min.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- jQuery.filer -->
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
<!-- Validator -->
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<!-- Ckeditor -->
<script src="{{ asset(url('plugins/ckeditor/ckeditor.js')) }} "></script>
<!-- tags -->
<script src="{{ asset(url('js/tags/jquery.tagsinput.min.js')) }}"></script>
<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>
<script type="text/javascript">
    var config = {
            // plugins: 'wysiwygarea,toolbar,basicstyles,menubutton,link,sourcearea',
            // extraPlugins: 'widgetbootstrap',
            toolbarGroups: [
                //{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                //{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                //{ name: 'forms', groups: [ 'forms' ] },
                // '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'align', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                // '/',
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                // { name: 'others', groups: [ 'others' ] }
            ]
        };
    config.removeButtons = 'Save,NewPage,Preview,Print,Templates';
    //config['height'] = 300;		

    CKEDITOR.replace( 'content', config);
    $(function(){
    	$('#tags').tagsInput({
	        width: 'auto'
	    });
	    function sansAccent(str){
		    var accent = [
		        /[\300-\306]/g, /[\340-\346]/g, // A, a
		        /[\310-\313]/g, /[\350-\353]/g, // E, e
		        /[\314-\317]/g, /[\354-\357]/g, // I, i
		        /[\322-\330]/g, /[\362-\370]/g, // O, o
		        /[\331-\334]/g, /[\371-\374]/g, // U, u
		        /[\321]/g, /[\361]/g, // N, n
		        /[\307]/g, /[\347]/g // C, c
		    ];
		    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
		    for(var i = 0; i < accent.length; i++){
		        str = str.replace(accent[i], noaccent[i]);
		    }
		    return str;
		}
	    $("#title").keyup(function(){
	        var str = sansAccent($(this).val());
	        str = $.trim(str);
	        str = str.replace(/[^a-zA-Z0-9\s]/g,"");
	        str = str.toLowerCase();
	        str = str.replace(/\s/g,'-');
	        $("#slug").val(str);
	    });
    });
</script>
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	    $('#datatable-responsive').DataTable({
			keys: true,
			sort: true
		});
	});
</script>
@endif
@endsection
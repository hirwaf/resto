<?php
	$in = session()->has('errors') ? true : false;
?>
@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a href="#addevent" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="{{ $edit || $in ? 'true' : 'false' }}" aria-controls="addevent"  >
						{{ $title }} Events
					</a> 
					<small>For Toundra Coffee Cup</small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div  id="addevent" class="x_content panel-collapse collapse {{ $edit || $in ? 'in' : '' }}" role="tabpanel">
				{!! Form::open([ 'route' => $route, 'files' => true, 'class' =>	'form-horizontal form-label-left' ]) !!}
					@if( $edit )
						{{ Form::hidden('id',$event->id) }}
					@endif
					<div class="form-group">
						{{ Form::label('title','Event Title *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('title', $edit ? $event->title : old('title') ,['class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('date','Event Date *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('date', $edit ? $event->date : old('date') ,['class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('duration','Event Starts *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="start" id="duration" class="form-control" value="{{ $edit ? $event->start : old('start') }}" required="" autocomplete="off" data-inputmask="'mask': '99:99:99'" >
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('end','Event Ends *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="end" id="end" class="form-control" value="{{ $edit ? $event->end : old('end') }}" required="" autocomplete="off" data-inputmask="'mask': '99:99:99'" >
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('entrence','Event Entrence *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('entrence', $edit ? $event->entrence : old('entrence') ,['class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ]) }}
							<small class="text text-xs text-info">Fill in <b>0</b> when <b>Free</b> and <b>1</b> when is a <b>drink</b> <br> The amount you fill in will be counted as <b>RWF</b> </small>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('image','Event Image *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::file('image',['class' => 'form-control', 'autocomplete' => 'off', "".$edit ? 'data' : 'required'."" => '' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('summary','Event Description *',['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::textarea('summary', $edit ? $event->summary : old('summary') ,['class' => 'form-control', 'rows' => '3', 'required' => '' ]) }}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 col-sm-3 col-xs-12"></div>
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{  Form::submit($button,['class' => 'btn btn-primary btn-md']) }}
						</div>
					</div>					
				{!! Form::close() !!}
			</div>
		</div>
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<a href="#allevents" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="{{ $edit || $in ? 'false' : 'true' }}" aria-controls="allevents"  >
						Alll Events
					</a> 
					<small>For Toundra Coffee Cup</small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div  id="allevents" class="x_content panel-collapse collapse {{ $edit || $in ? '' : 'in' }}" role="tabpanel">
				<ul class="list-unstyled timeline">
					@if( count($events) >= 1 && is_object($events) )
						@foreach( $events as $event )
			                <li>
			                    <div class="block">
			                      <div class="tags">
			                        <a href="" class="tag">
			                          <span>
			                          	<?php
			                          		$f = $event->date;
			                          		$x = explode('-', $f);
			                          		$d = explode(' ', $x[2]);
			                          		echo $x[1]."/".$d[0]."/".$x[0];

			                          	?>
			                          </span>
			                        </a>
			                      </div>
			                      <div class="block_content">
			                        <h2 class="title">
                                        <a>{{ strtoupper($event->title) }}</a>
                                    </h2>
			                        <div class="byline">
			                          <span>DURATION : </span><a>{{ $event->start . " - " . $event->end   }}</a>
			                        </div>
			                        <div class="byline">
			                          <span>PRICE : </span><a>{{ $event->entrence == 0 ? "Free" : $event->entrence == 1 ? "Drink" : $event->entrence." RWF"    }}</a>
			                        </div>
			                        <p class="excerpt">
			                        	<div class="small text text-sm">
			                        		{{ $event->summary }}
			                        	</div>
				                        <hr>
			                        	<a  class="pull-right" style="color: #3498DB;" href="{{ url(route('edit.event.admin',$event->id)) }}" >Edit</a>

			                        	<a  class="pull-left cancel" style="color: #C0392B; cursor: pointer;" data-event="{{ $event->id }}" >Cancel</a>
			                        	<div class="clearfix">&nbsp;</div>
			                        </p>
			                      </div>
			                    </div>
			                </li>
		                @endforeach
	            </ul>
				<nav>
				  <ul class="pager primary">
				    <li><a href="{{ $events->previousPageUrl() }}">Previous</a></li>
				    <li>&nbsp;</li>
				    <li> <label class="label label-default"> Total:{{ $events->total() }} | Current:{{ $events->currentPage() }} </label> </li>
				    <li>&nbsp;</li>
				    <li><a href="{{ $events->nextPageUrl() }}">Next</a></li>
				  </ul>
				</nav>
                @endif
			</div>
		</div>
	</div>
</div>
<!-- Start Calender modal -->
<?php
	$token = str_random(40);
	session(['destroy' => $token]);
?>
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
@endsection
@section('scrpits')
<!-- pace -->
<script src="{{ asset(url('js/pace/pace.min.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- jQuery.filer -->
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
<!-- Validator -->
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<!-- input mask -->
<script src="{{ asset(url('js/input_mask/jquery.inputmask.js')) }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ asset(url('js/moment/moment.min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/datepicker/daterangepicker.js')) }}"></script>
<!-- Errors -->
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: false,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
<!-- Validate Form -->
<script>
$(document).ready(function() {
  $(":input").inputmask();
  $('#date').daterangepicker({
    singleDatePicker: true,
    format: 'YYYY-MM-DD',
    calender_style: "picker_1"
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });

  $("*.cancel").on('click',function(e){
  	var k = $(this).attr("data-event");
  	if ( confirm("Are sure \nYou want to cancel this event !! ") == true ) {
  		$.post(
	  		"{{ url(route('destroy.event.admin')) }}",
	  		{
	  			id : k,
	  			_token : "{{ csrf_token() }}"
	  		},function(data){
	  			window.location = data;
	  		}
	  	);
  	}
  	else{
  		return false;
  	};
  });

});
</script>
<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>


@endsection
@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
	<?php
		$repo = new \App\Repositories\PageRepository;
	?>
	@if($slug == 'welcome')
		@include('admin.layouts.pages.home', [ 'slides'=>$repo->getSlides($slug) ])
	@elseif($slug == 'gallery')
		@include('admin.layouts.pages.gallery', [ 'images' => $repo->getGalleries(),'categories' => $repo->getCategories()  ] )
	@elseif($slug == 'about-us' )
		@include('admin.layouts.pages.about', [ 'about' => $repo->getAboutUs($slug) ])
	@endif
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
<!-- select2 -->
<link href="{{ asset(url('css/select/select2.min.css')) }}" rel="stylesheet">
@endsection

@section('scrpits')
@if(!$token)
<script type="text/javascript">
	window.location="{{ url(route('pages.admin')) }}"; 
</script>
@endif
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- jQuery.filer -->
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
<!-- select2 -->
<script src="{{ asset(url('js/select/select2.full.js')) }}"></script>
@endsection
@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<div class="x_panel">
			<div class="x_title" role="tab" id="headingAll">
				<h2>
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					List of Employees
					</a>
					<small> For Toundra coffee cup </small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content collapse in" id="collapseOne"  role="tabpanel" aria-labelledby="headingAll">
				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<th>Employees</th>
						<th></th>
					</thead>
					<tbody>
						@if( count($employees) >= 1  )
							@foreach( $employees as $employee )
								<tr>
									<td>
										<div class="row">
											<div class="col-sm-2">
												<img src="{{ asset(url('images/web/employees/'.$employee->image)) }}" class="img-responsive img-thumbnail" >
											</div>
											<div class="col-sm-9">
												<h3>{{ ucfirst($employee->firstname)." ".ucfirst($employee->lastname) }}</h3>
												<p>{{ $employee->note }}</p>
												<div class="label label-primary">Email : {{$employee->email}}</div>
												<div class="label label-primary">Telephone : {{$employee->telephone}}</div>
												<div class="label label-primary">Nation ID : {{$employee->nation_id}}</div>
												<div class="label label-primary">Gender : {{ ucfirst($employee->gender) }}</div>
											</div>
										</div>
									</td>
									<td>
										<a href="{{ url(route('edit.employee.admin',$employee->id)) }}" class="btn btn-xs btn-block btn-success" >Edit</a>
										<a href="{{ url(route('create.massege.admin',[ 'sms',$employee->id,session()->get('token_msg') ] )) }}" class="btn btn-xs btn-block btn-info btn-sms">Send SMS</a>
										<a href="{{ url(route('create.massege.admin',[ 'eml',$employee->id,session()->get('token_msg') ] )) }}" class="btn btn-xs btn-block btn-info btn-email" >Send E-Mail</a>
										<button role='button' class="btn btn-xs btn-block btn-danger btn-del" data-employee="{{ $employee->id }}" data-type-e='n' >Delete</button>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
		<!-- Second Panel -->
		<div class="x_panel">
			<div class="x_title" role="tab" id="headingFired">
				<h2>
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						List of Fired Employees 
					</a>
					<small> For Toundra coffee cup </small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div id="collapseTwo" class="x_content collapse" role="tabpanel" aria-labelledby="headingFired">
				<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<th>Employees</th>
						<th></th>
					</thead>
					<tbody>
						@if( count($history) >= 1  )
							@foreach( $history as $employee )
								<tr>
									<td>
										<div class="row">
											<div class="col-sm-2">
												<img src="{{ asset(url('images/web/employees/'.$employee->image)) }}" class="img-responsive img-thumbnail" >
											</div>
											<div class="col-sm-9">
												<h3>{{ ucfirst($employee->firstname)." ".ucfirst($employee->lastname) }}</h3>
												<p>{{ $employee->note }}</p>
												<div class="label label-primary">Email : {{$employee->email}}</div>
												<div class="label label-primary">Telephone : {{$employee->telephone}}</div>
												<div class="label label-primary">Nation ID : {{$employee->nation_id}}</div>
												<div class="label label-primary">Gender : {{ ucfirst($employee->gender) }}</div>
											</div>
										</div>
									</td>
									<td>
										<a role='button' data-employee="{{ $employee->id }}" class="btn btn-xs btn-block btn-success btn-res" >Restore</a>
										<button role='button' class="btn btn-xs btn-block btn-danger btn-del" data-employee="{{ $employee->id }}" data-type-e='p'  >Delete</button>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scrpits')
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- Datatables-->
<script src="{{ asset(url('js/datatables/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.bootstrap.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.buttons.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/jszip.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/pdfmake.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/vfs_fonts.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/buttons.print.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.fixedHeader.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.keyTable.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/responsive.bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/datatables/dataTables.scroller.min.js')) }}"></script>
<script type="text/javascript">
	$(function(){
		$('#datatable-responsive').DataTable({
			keys: true,
			sort: false
		});
		$('#datatable').DataTable({
			keys: true,
			sort: false
		});
		$("*.btn-del").on('click',function(e){
			var v = $(this).attr("data-employee");
			var type = $(this).attr('data-type-e');

			if ( confirm("Are sure \nYou want to delete this employee !! ") == true ) {
		  		$.post(
			  		"{{ url(route('destroy.employee.admin')) }}",
			  		{
			  			id : v,
			  			type : type,
			  			_token : "{{ csrf_token() }}"
			  		},function(data){
			  			window.location = data;
			  		}
			  	);
		  	}
		  	else{
		  		return false;
		  	};
		});
		$("*.btn-res").on('click',function(e){
			var v = $(this).attr("data-employee");

			if ( confirm("Are sure \nYou want to restore this employee !! ") == true ) {
		  		$.post(
			  		"{{ url(route('restore.employee.admin')) }}",
			  		{
			  			id : v,
			  			_token : "{{ csrf_token() }}"
			  		},function(data){
			  			window.location = data;
			  		}
			  	);
		  	}
		  	else{
		  		return false;
		  	};
		});
	});
</script>
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	    $('#datatable-responsive').DataTable({
			keys: true,
			sort: true
		});
	});
</script>
@endif
@endsection
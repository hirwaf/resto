@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					{{ $title }} Employee
					<small> For Toundra coffee cup </small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content">
				{!! Form::open(['route' => $route, 'files' => true ,'class' => 'form-horizontal form-label-left']) !!}
					@if( $edit )
						{{ Form::hidden('id',$employee->id) }}
					@endif
					<div class="form-group">
						{{ Form::label('fistname',"Firstname *", [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('firstname', $edit ? $employee->firstname : old('firstname'), [ 'class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ])  }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('lastname',"Lastname *", [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('lastname', $edit ? $employee->lastname : old('lastname'), [ 'class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ])  }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('gender',"Gender *", [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<label>
								<input type="radio"  required name="gender" value="male" <?= $edit ? $employee->gender == 'male' ? "checked" : ""  : old('gender') == 'male' ? "checked" : "" ?> >
								Male
							</label>
							&nbsp;
							<label>
								<input type="radio"  required name="gender" value="female" <?= $edit ? $employee->gender == 'female' ? "checked" : ""  : old('gender') == 'female' ? "checked" : "" ?> >
								Felemale
							</label>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('nation_id','National ID *', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="nation_id" id="nation_id" class="form-control" data-inputmask="'mask' : '* **** * ******* * **'" value="{{ $edit ? $employee->nation_id : old('nation_id') }}" required="" >
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('telephone', 'Telephone *', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="telephone" id="telephone" class="form-control" data-inputmask=" 'mask' : '250 *** *** ***' " value="{{ $edit ? $employee->telephone : old('telephone') }}" required="" >
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('email', 'Email *', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::email('email', $edit  ? $employee->email : old('email'), [ 'class' => 'form-control', 'autocomplete' => 'off', 'required' => '' ] ) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('post', 'Post *', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('post', $edit  ? $employee->post : old('post'), [ 'class' => 'form-control' ] ) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('chef', " Chef " ,[ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							<label>
								{{ Form::checkbox('chek',true,  $edit  ? $employee->note ? 'checked' : '' : old('note') ? 'checked' : '' ) }}
							</label>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('note', "Note", [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::textarea('note', $edit  ? $employee->note : old('note'), [ 'class' => 'form-control', 'rows' => '3' ]) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('image', 'Picture',[ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::file('image',['class' => 'form-group']) }}
							@if( $edit )
								<span class="text-info text-md">
								@if( $employee->image != null )
									<button type="button" class="btn btn-xs btn-view btn-link" data-image="{{ $employee->image }}" >View {{ $employee->gender == 'male' ? "His " : "Her " }} Picture  </button>
								@else
									<i>{{ $employee->gender == 'male' ? "He " : "She " }} don't have a picture !! </i>
								@endif
								</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('', 'Social Network',[ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ]) }}
						<div class="col-md-8 col-sm-8 col-xs-12">
							{{ Form::text('facebook', $edit ? empty(json_decode($employee->social_networks,true)) ? "" : isset(json_decode($employee->social_networks,true)['facebook'])? json_decode($employee->social_networks,true)['facebook'] :""  : old('facebook'), [ 'class' => 'form-control', 'placeholder' => 'Fecabook' ] ) }}
							<div class="clearfix">&nbsp;</div>
							{{ Form::text('twitter', $edit ? empty(json_decode($employee->social_networks,true)) ? "" : isset(json_decode($employee->social_networks,true)['twitter'])? json_decode($employee->social_networks,true)['twitter'] :""  : old('twitter'), [ 'class' => 'form-control', 'placeholder' => 'Twitter' ] ) }}
							<div class="clearfix">&nbsp;</div>
							{{ Form::text('linkedin', $edit ? empty(json_decode($employee->social_networks,true)) ? "" : isset(json_decode($employee->social_networks,true)['linkedin'])? json_decode($employee->social_networks,true)['linkedin'] :""  : old('linkedin'), [ 'class' => 'form-control', 'placeholder' => 'LinkedIn' ] ) }}
							<div class="clearfix">&nbsp;</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 col-sm-3 col-sm-offset-3 col-xs-12">
							{{ Form::submit($button,[ 'class' => 'btn btn-primary btn-block' ]) }}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<div class="modal fade ">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
      	<img src="" class="img-responsive img-rounded">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
@endsection

@section('scrpits')
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- pace -->
<script src="{{ asset(url('js/pace/pace.min.js')) }}"></script>
<!-- jQuery.filer -->
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
<!-- Validator -->
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<!-- input mask -->
<script src="{{ asset(url('js/input_mask/jquery.inputmask.js')) }}"></script>
<script type="text/javascript">
	$(function(){
		$(":input").inputmask();
		$('*.btn-view').on('click', function(e){
			var v = $(this).attr('data-image');
			var modal = $(".modal");
			modal.find(".modal-body img").attr('src',"{{ asset(url('images/web/employees')) }}/"+v);
			modal.modal();
		});
	});
</script>
<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>

@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	    $('#datatable-responsive').DataTable({
			keys: true,
			sort: true
		});
	});
</script>
@endif
@endsection
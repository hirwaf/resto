@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Site Background color & images 
					<small>For Toundra Coffee Cup</small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content">
				{{ Form::open(['route' => 'store.style.cogs.admin', 'files' => true, 'class' => 'form-horizontal form-label-left' ]) }}
					<div class="form-group">
                      {{ Form::label('top', 'Top Header', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="input-group demo2 ">
                          {{ Form::text('top', $cog->getStyle('top'), [ 'class' => 'form-control', 'required' => '' ] ) }}
                          <span class="input-group-addon"><i></i></span>
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      {{ Form::label('nav', 'Nav items ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="input-group demo2 ">
                          {{ Form::text('nav', $cog->getStyle('nav'), [ 'class' => 'form-control', 'required' => '' ] ) }}
                          <span class="input-group-addon"><i></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      {{ Form::label('size', 'Header Size ', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        	{{ Form::select('size', array( 'large' => 'Large', 'mediam' => 'Mediam', 'small' => 'Small' ), $cog->getStyle('header-size'), ['class' => 'form-control']) }}
                      </div>
                    </div>
                    @if( count($pages) >= 1 )
                    	@foreach( $pages as $page )
                    		@if( $page->slug == 'welcome' )
                    			<?php continue; ?>
                    		@endif
                    		<div class="form-group">
		                      {{ Form::label('', ucfirst($page->name), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'] ) }}
		                      <div class="col-md-9 col-sm-9 col-xs-12">
		                          {{ Form::file($page->slug, [ 'class' => 'form-control', 'id' => 'image' ] ) }}
		                          <span class="text-info text-xs btn-view" style="cursor: pointer;" data-image="{{ $cog->getStyle($page->slug) }}" data-name="{{ ucfirst($page->name) }}" >View</span> <small class="text-muted">Current Background Image</small>
		                      </div>
		                    </div>
                    	@endforeach
                    @endif
                    <div class="form-group">
                    	<div class="col-md-9 col-sm-9 col-md-offset-3 col-sm-offset-3 col-xs-12">
                    		{{ Form::submit('Save changes', ['class' => 'btn btn-primary']) }}
                    	</div>
                    </div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
<div class="modal fade">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      	<h4></h4>
      </div>
      <div class="modal-body">
      	<img src="" class="img-responsive img-rounded" >
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('styles')
<!-- colorpicker -->
<link href="{{ asset(url('css/colorpicker/bootstrap-colorpicker.min.css')) }}" rel="stylesheet">
<!-- jQuery-filer -->
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('js/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />

@endsection

@section('scrpits')
<!-- Validator -->
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<!-- color picker -->
<script src="{{ asset(url('js/colorpicker/bootstrap-colorpicker.min.js')) }}"></script>
<script src="{{ asset(url('js/colorpicker/docs.js')) }}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>
<!-- jQuery Filer -->
<script src="{{ asset(url('js/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('js/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>

<script type="text/javascript">
	$(function(){
		$("*.btn-view").on('click', function(e){
			var u = $(this).attr('data-image');
			var p = $(this).attr('data-name');
			var modal = $(".modal");
			modal.find('.modal-body img').attr('src',"{{ asset(url('images/web')) }}/"+u);
			modal.find('.modal-header h4').text(p+" Page");
			modal.modal();
		});
	});
</script>
<!-- Validate Form -->
<script>
	// initialize the validator function
	validator.message['date'] = 'not a real date';
	$("* input ").attr('autocomplete','off');
	// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
	$('form')
	  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
	  .on('change', 'select.required', validator.checkField)
	  .on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required')
	  .on('keyup blur', 'input', function() {
	    validator.checkField.apply($(this).siblings().last()[0]);
	  });

	// bind the validation to the form submit event
	//$('#send').click('submit');//.prop('disabled', true);

	$('form').submit(function(e) {
	  e.preventDefault();
	  var submit = true;
	  // evaluate the form using generic validaing
	  if (!validator.checkAll($(this))) {
	    submit = false;
	  }

	  if (submit)
	    this.submit();
	  return false;
	});
</script>
@if ( session()->has('errors') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Error",
	        type: "error",
	        text: "\
	        \ @foreach( session()->pull('errors') as $error ) \
	        \ 	{{ $error }} \n \
	        \ @endforeach \
	        ",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	});
</script>
@endif
@if ( session()->has('success') )
<script type="text/javascript">
	$(function(){
		new PNotify({
	        title: "Done",
	        type: "success",
	        text: "{{ session()->pull('success') }}",
	        hide: true,
	        nonblock: {
	          nonblock: true
	        }
	    });
	    $('#datatable-responsive').DataTable({
			keys: true,
			sort: true
		});
	});
</script>
@endif
@endsection
@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection
@section('content')
	<div class="left_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
	            <h3 style="
	            	text-transform: capitalize;
	            ">
	                {{$get->admin('name')}} profile
	            </h3>
            </div>
          </div>

          <div class="clearfix"></div>
			<div class="row">
		        <div class="col-md-12 col-sm-12 col-xs-12">
		          <div class="x_panel">
		            <div class="x_content">

		              <form class="form-horizontal form-label-left" novalidate method="post" action="{{ url(route('post.profile.admin')) }}" >
		                {{ csrf_field() }}
		                <div class="row">
		                	@if(session()->has('errors'))
			                <div class="col-sm-6 col-sm-offset-3">
			                	@foreach(session()->pull('errors') as $error)
			                	<div class="alert alert-danger alert-dismissible fade in" role="alert">
				                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                  	<span aria-hidden="true">×</span>
				                  </button>
				                  {{ $error }}
				                </div>
				                @endforeach
			                </div>
			                @endif
		                </div>
		                <div class="item form-group">
		                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name 
		                  <span class="required">*</span>
		                  </label>
		                  <div class="col-md-6 col-sm-6 col-xs-12">
		                    <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Hirwa Felix" required="required" type="text" value="{{ $get->admin('name') }}">
		                  </div>
		                </div>
		                <div class="item form-group">
		                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username 
		                  <span class="required">*</span>
		                  </label>
		                  <div class="col-md-6 col-sm-6 col-xs-12">
		                    <input id="username" class="form-control col-md-7 col-xs-12" data-validate-length-range="3" data-validate-words="1" name="username" placeholder="e.g hirwaf" required="required" type="text" value="{{ $get->admin('username') }}">
		                  </div>
		                </div>
		                <div class="item form-group">
		                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
		                  </label>
		                  <div class="col-md-6 col-sm-6 col-xs-12">
		                    <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $get->admin('email') }}" >
		                  </div>
		                </div>
		                <div class="item form-group">
		                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telephone <span class="required">*</span>
		                  </label>
		                  <div class="col-md-6 col-sm-6 col-xs-12">
		                    <input type="tel" id="telephone" name="telephone" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" value="{{ $get->admin('telephone') }}">
		                  </div>
		                </div>
		                <div style="
		                	background-color: #ECF0F1;
		                	padding: 15px;
		                ">
			                <div class="item form-group">
			                  <label for="password" class="control-label col-md-3">Password</label>
			                  <div class="col-md-6 col-sm-6 col-xs-12">
			                    <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" >
			                  </div>
			                </div>
			                <div class="item form-group">
			                  <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password</label>
			                  <div class="col-md-6 col-sm-6 col-xs-12">
			                    <input id="password2" type="password" name="password_confirmation" data-validate-linked="password" class="form-control col-md-7 col-xs-12">
			                  	<span class="text-info text-sm">Please leave these two fields <i class="fa fa-arrow-up"></i> empty if no changes</span>
			                  </div>
			                </div>
		                </div>
		                <div class="ln_solid"></div>
		                <div class="item form-group">
		                  <label for="password" class="control-label col-md-3">Current Password
		                  	<span class="required">*</span>
		                  </label>
		                  <div class="col-md-6 col-sm-6 col-xs-12">
		                    <input id="password" type="password" name="currentpassword" data-validate-length="6,8" class="form-control col-md-7 col-xs-12">
		                  	<span class="text-info text-xs"> Enter your current password to save changes. </span>
		                  </div>
		                </div>
		                <div class="ln_solid"></div>
		                <div class="form-group">
		                  <div class="col-md-6 col-md-offset-3">
		                    <button id="send" type="submit" class="btn btn-success">Submit</button>
		                  </div>
		                </div>
		              </form>

		            </div>
		          </div>
		        </div>
		    </div>          
		</div>  
    </div>
@endsection
@section('scrpits')
<script src="{{ asset(url('js/validator/validator.js')) }}"></script>
<script>
// initialize the validator function
validator.message['date'] = 'not a real date';
$("* input ").attr('autocomplete','off');
// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
$('form')
  .on('blur', 'input[required], input.optional, select.required', validator.checkField)
  .on('change', 'select.required', validator.checkField)
  .on('keypress', 'input[required][pattern]', validator.keypress);

$('.multi.required')
  .on('keyup blur', 'input', function() {
    validator.checkField.apply($(this).siblings().last()[0]);
  });

// bind the validation to the form submit event
//$('#send').click('submit');//.prop('disabled', true);

$('form').submit(function(e) {
  e.preventDefault();
  var submit = true;
  // evaluate the form using generic validaing
  if (!validator.checkAll($(this))) {
    submit = false;
  }

  if (submit)
    this.submit();
  return false;
});
</script>
@endsection
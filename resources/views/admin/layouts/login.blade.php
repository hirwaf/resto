<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Toundra - Admin Login </title>

  <!-- Bootstrap core CSS -->

  <link href="{{ asset(url('css/bootstrap.min.css')) }}" rel="stylesheet">

  <link href="{{ asset(url('fonts/css/font-awesome.min.css')) }}" rel="stylesheet">
  <link href="{{ asset(url('css/animate.min.css')) }}" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="{{ asset(url('css/custom.css')) }}" rel="stylesheet">
  <link href="{{ asset(url('css/icheck/flat/green.css')) }}" rel="stylesheet">


  <script src="{{ asset(url('js/jquery.min.js')) }}"></script>
  <script type="text/javascript" src="{{ asset(url('js/notify/pnotify.core.js')) }}"></script>
  <script type="text/javascript" src="{{ asset(url('js/notify/pnotify.buttons.js')) }}"></script>
  <script type="text/javascript" src="{{ asset(url('js/notify/pnotify.nonblock.js')) }}"></script>

  <!--[if lt IE 9]>
  <script src="../assets/js/ie8-responsive-file-warning.js"></script>
  <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">

        <section class="login_content">
          {!! Form::open(array('url' => route('post.login.admin'))) !!}
            <h1>Login Form</h1>
            <div>
              {!! Form::text('credential', '',['class' => 'form-control','placeholder' => 'Username, Email or Telephone','required' => true ]) !!}
            <div>
              {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password','required' => true ] )  !!}
            </div>
            <div>
              {!! Form::submit('Log in',['class' => 'btn btn-default submit']) !!}
              <!-- <a class="" href="index.html">Log in</a> -->
              <a class="reset_pass" href="#">Lost your password?</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
            <div class="clearfix"></div>
              <br />
              <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i>Toundra</h1>

                <p>©{{ date('Y') }} All Rights Reserved Toundra coffee cup. Developed By <a href="http://www.elix.com">hirwaf</a> </p>
              </div>
            </div>
          {!! Form::close() !!}
          <!-- form -->
        </section>
        <!-- content -->
      </div>
    </div>
  </div>
  @if( old('credential') != null )
     <script type="text/javascript">
      var permanotice, tooltip, _alert;
      $(function() {
        new PNotify({
          title: "Error",
          type: "notice",
          text: "Incorrect Credentials",
          nonblock: {
            nonblock: true
          }
        });
      });
    </script>
  @endif
</body>

</html>

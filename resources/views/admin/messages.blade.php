@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Customer Messages
					<small>For Toundra Coffee Cup</small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content">
				@if( count( $get->getMessages() ) >= 1 )
	                <ul class="list-unstyled msg_list">
	                @foreach( $get->getMessages() as $massage  )
		                <li style="{{  $massage->read ? '' : 'background-color: #BDC3C7;'  }}" class="btn-read" data-message="{!! $massage->message !!}" data-name="{{ $massage->name }}" data-id="{{ $massage->id }}"  >
		                    <a>
		                      	<span class="image"></span>
		                      	<span>
			                        <span style="text-transform: capitalize;">{{ $massage->name }}</span>
			                      	<span class="time" style="margin-right: 1em;">{{ $massage->created_at->diffForHumans() }}</span>
		                      	</span>
		                      	<div class="clearfix">&nbsp;</div>
		                      	<span class="message">
			                        {{ substr($massage->message,0,100)." ..." }}
			                    </span>
		                    </a>
		                </li>
		                <?php $get->makeSeen($massage->id) ?>
		            @endforeach    
	                </ul>
	                {!! $get->getMessages()->links() !!}
	            @else
	            	<h2>No Message avaible !!</h2>    
                @endif
              </div>
		</div>
	</div>
</div>
<div class="modal fade">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      	<h4></h4>
      </div>
      <div class="modal-body">
      	<p></p>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scrpits')
<script type="text/javascript">
	$(function(){
		$("*.btn-read").on('click', function(e){
			var m = $(this).attr('data-message');
			var p = $(this).attr('data-name');
			var id = $(this).attr('data-id');
			var modal = $(".modal");
			modal.find('.modal-body p').html(m);
			modal.find('.modal-header h4').text(p);
			modal.modal();
			$.post("{{ url(route('message.read.admin')) }}",
				{
					id: id,
					_token : "{{ csrf_token() }}"
				},
				function(data){

				}
			);
		});
	});
</script>
@endsection

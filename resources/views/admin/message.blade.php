@extends('admin.app')
@section('title',strtoupper($get->admin('name'))." - Toundra Coffee Cup")

@section('sidebar')
	@include('admin.sidebar')
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Send {{ $type == 'sms' ? " a SMS" : "an E-Mail" }} To {{ ucfirst($employee->firstname)." ".ucfirst($employee->lastname) }}
					<small>For Toundra Coffee Cup</small>
				</h2>
				<div class="clearfix">&nbsp;</div>
			</div>
			<div class="x_content">
				<?php
					$to = $type == 'sms' ? "+".str_replace(" ", "", $employee->telephone) : $employee->email;
				?>
				{{ Form::open( [ 'route' => $route ] ) }}
					<div class="form-group">
						{{ Form::label('to','To',[ 'class' => 'control-label col-sm-3' ]) }}
						<div class="col-sm-9">
							{{ Form::text('to', $to, [ 'class' => 'form-control', 'readonly' => '' ]) }}
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="form-group">
						{{ Form::label('msg', 'Message', ['class' => 'control-label col-sm-3'] ) }}
						<div class="col-sm-9">
							{{ Form::textarea('msg', null, ['class' => 'form-control', 'rows' => '6' ]) }}
						</div>
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3">
							{{ Form::submit('Send', [ 'class' => 'btn btn-block btn-sm btn-primary' ]) }}
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection
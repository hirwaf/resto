@extends('app')

@section('title',"Contact-Us")

@section('main-nav')
    @include('main_nav')
@endsection

@section('content')
	<div class="section dark transparent parallax" style="background-image: url('{{ asset(url('images/web/'.$bg)) }}');">
	<div class="inner">
		<div class="container">
			<div class="contact-wrapper">
				@if( isset($video) )
				<div class="row">
					<div class="col-sm-9">
						<div class='embed-responsive embed-responsive-16by9'>
							<iframe class='embed-responsive-item' src="{{ $video->net_url }}"></iframe>
						</div>
					</div> <!-- end .col-sm-6 -->
				</div> <!-- end .row -->
				<div class="clearfix">&nbsp;</div>
				@endif
				@if( count( $page->getVideos() ) >= 1 )
				<div class="row">
					<div class="col-md-12">
						<div class="row">
						@foreach( $page->getVideos() as $video )
							<div class="col-xs-6 col-md-2">
						    	<a href="{{ url(route('view.video.web',$video->id)) }}" class="thumbnail">
						      		<img src="{{ url($video->name) }}" alt="{{ ucfirst($video->title) }}" class="img-responsive" >
						    	</a>
						    	<div class="caption">
						    		<h6 style="text-transform: capitalize;" class="text-muted" >{{ $video->title }}</h6>					    		
						    	</div>
						  	</div>
						@endforeach
						</div>
						{!! $page->getVideos()->links() !!}
					</div>
				</div>
				@endif
				<!-- <div id="map" class="map"></div> -->
			</div> <!-- end .contact-wrapper -->
		</div> <!-- end .container -->
	</div> <!-- end .inner -->
</div> <!-- end .section -->
<style type="text/css">
	body{
		overflow-x: hidden;
	}
</style>
@endsection